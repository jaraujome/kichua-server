Este Server por ahora contiene una API-REST  utilizando nodejs y mongodb , acá lo que tenemos 
ahora es la autenticación , la cual se maneja a través de un concepto llamado JsonWebToken , 
adicionalmente a la auth vamos a implementar roles , en este caso los asumidos hasta ahora son admin 
y usuario , para hacer esto necesitamos crear un modelo y relacionarlo en la base de datos , 

1-Descargar e instalar node v12.16.1.
enlace:
Windows 32-bit Installer: https://nodejs.org/dist/v12.16.1/node-v12.16.1-x86.msi
Windows 64-bit Installer: https://nodejs.org/dist/v12.16.1/node-v12.16.1-x64.msi

2-Instalar VisualStudio Code
enlace:
https://code.visualstudio.com/docs/?dv=win64user

3-clonar el repo desde GitHub usando la terminal.

4-Una vez clonado el repo , entonces arrastramos la carpeta del repo a el VisualStudioCode 

5-Una vez arrastrada la carpeta , entonces , procedemos a instalar las dependencias de nodejs que nos permiten ejecutar la app , 
estas de pendecias las instalamos a traves del gestor de paquetes de nodejs npm ,para ello damos click en la pesta�a terminal del 
visual studio code , y cuando se nos abra el terminal entonces ejecutamos este comando. 
------------------------------------ Comando para instalar dependencias--------------------------------------------------------------------------
npm i bcryptjs body-parser cors dotenv ejs express helmet html-pdf jsonwebtoken mongoose morgan multer react-dom react-native-web
-------------------------------------------------------------------------------------------------------------------------------------------------
6- Una vez que se haga esto , es necesario instalar mongodb en su ordenador  ,  para ello crear una carpeta en C: llamada data o sea C:/data , 
luego descargar el instalador de mongodb para Windows .  una vez instalado mongodb volvemos a ir a la consola de visual studio y ejecutamos el 
comando mongod , en una consola y abrimos otra para ejecutar el proyecto , todo , en el mismo visual studiocode , a continuacion muestro la consolas 

7- Luego en cmd 1 ejecutamos el comando npm run dev y en cmd 2 el comando mongod. 



