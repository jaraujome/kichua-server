import mongoose from 'mongoose'

mongoose.set('useCreateIndex', true);

mongoose.connect("mongodb://localhost/kichua",{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true,
    
})
      .then(db => console.log('Base de Datos Conectada'))
      .catch(error => console.log(error))