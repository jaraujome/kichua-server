import { Schema, model } from 'mongoose'
import { LeccionSchema } from './Leccion'

const RecursoSchema = new Schema({
    name: { type: String },
    url: { type: String },
});

const DiccionarioSchema = new Schema({
    es: { type: String },
    kichua: { type: String },
});

const NivelSchema = new Schema({
    numero: { type: Number, required: false, unique: false },
    nombre: { type: String, required: true, unique: false },
    diccionario: { type: [DiccionarioSchema] },
});

const PreguntaSchema = new Schema({
    numero: { type: Number, required: false, unique: false },
    texto: { type: String, required: true, unique: false },
    tipo: { type: String, required: true, enum: ['Cuestionario', 'Completación'], default: 'Cuestionario' },
    respuestaCorrecta: { type: String, default: '' },
    respuesta: { type: String, default: '' },
    salto: { type: Number, default: 0 },
    nota: Number,
});

const LeccionInstance = new Schema({
    nota: { type: Number, default: 0 },
    numero: { type: Number, required: true, unique: false },
    nombre: { type: String, required: false, unique: false },
    icono: { type: String, required: false, unique: false },
    recursos: [RecursoSchema],
    actividad: String,
    niveles: [NivelSchema],
    evaluacion: [PreguntaSchema],
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    status: { type: String, required: true, enum: ['En proceso', 'Completado'], default: 'En proceso' },
}, { timestamps: true });

export default model('LeccionInstance', LeccionInstance);
