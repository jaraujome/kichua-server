import { Schema, model } from 'mongoose'

const RecursoSchema = new Schema({
    name: { type: String },
    url: { type: String },
});

const DiccionarioSchema = new Schema({
    es: { type: String },
    kichua: { type: String },
});

const NivelSchema = new Schema({
    numero: { type: Number, required: true, unique: false },
    nombre: { type: String, required: true, unique: false },
    diccionario: { type: [DiccionarioSchema] },
});

const PreguntaSchema = new Schema({
    numero: { type: Number, required: true, unique: false },
    texto: { type: String, required: true, unique: false },
    tipo: { type: String, required: true, enum: ['Cuestionario', 'Completación'], default: 'Cuestionario' },
    respuestaCorrecta: { type: String, default: '' },
    salto: { type: Number, default: 0 },
    nota: Number,
});

export const LeccionSchema = new Schema({
    numero: { type: Number, required: true, unique: false },
    nombre: { type: String, required: false, unique: false },
    icono: { type: String, required: false, unique: false },
    recursos: [RecursoSchema],
    actividad: String,
    niveles: [NivelSchema],
    evaluacion: [PreguntaSchema],
}, { timestamps: true });

export default model('Leccion', LeccionSchema);
