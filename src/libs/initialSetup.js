import Role from '../models/Role'
import User from '../models/User'
import Leccion from '../models/Leccion'

export const createRoles = async () => {
  try {
    const count = await Role.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      new Role({ name: "usuario" }).save(),
      new Role({ name: "admin" }).save(),
    ]);

    console.log(values);

  } catch (error) {
    console.error(error);
  }
};

export const createSuperUser = async () => {
  try {
    const emailAdmin = 'admin@kichua.com';
    const userSuperAdmin = await User.findOne({email: emailAdmin});
    
    if (userSuperAdmin) return;

    const newUser = new User({ username: 'admin', name: 'Admin', lastName: '', email: emailAdmin, })

    newUser.password = await newUser.encryptPassword('admin');

    const role = await Role.findOne({ name: "admin" });
    newUser.roles = [role._id];

    const savedUser = await newUser.save();

    console.log(savedUser);

  } catch (error) {
    console.error(error);
  }
};

export const createLeccions = async () => {
  try {
    const count = await Leccion.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      new Leccion(
        {
          numero: 1,
          nombre: 'Artículos, Pronombres etc',
          recursos: [
            {
              name: '1. EXPRESIONES UTILES',
              url: '1. EXPRESIONES UTILES.pdf'
            },
            {
              name: '2. PRONOMBRES PERSONALES',
              url: '2. PRONOMBRES PERSONALES.pdf'
            },
            {
              name: '3. ARTICULOS',
              url: '3. ARTICULOS.pdf'
            }
          ],
          actividad: `
          • Expresiones Útiles
          • Pronombres Personales
          • Artículos
      
          1. Actividad de la lección 1
            
            Objetivos: <br>
            En esta unidad aprenderá las expresiones comúnmente utilizadas en el idioma Kichwa, y las partes básicas de las oraciones como son los pronombres, y los artículos. 
            El estudiante logrará reconocer las expresiones en Kichwa y utilizarlas según el contexto requerido.
            
            Actividad:
                a. Revisar el contenido
                b. Apoyarse en los recursos para complementar el aprendizaje.
                c. Al finalizar, deberá responder el cuestionario para practicar lo aprendido.

          2. Recursos de apoyo de la lección
            Descárguese los pdf de cada uno de los temas aprendidos.
          `,
          niveles: [
            {
              numero: 1,
              nombre: 'EXPRESIONES ÚTILES (SALUDOS)',
              diccionario: [
                {
                  es: 'Buenos Días',
                  kichua: 'Alli punlla',
                },
                {
                  es: 'Buenas Tardes',
                  kichua: 'Alli chishi',
                },
                {
                  es: 'Buenas Noches',
                  kichua: 'Alli tuta',
                },
                {
                  es: '¿Cuál es tu nombre?',
                  kichua: 'Ima shuti kanki',
                },
                {
                  es: 'Mi nombre es:',
                  kichua: 'Ñuka shutimi kan:',
                },
                {
                  es: '¿Cómo estás?',
                  kichua: 'Imanalla kanki',
                },
                {
                  es: 'Bien',
                  kichua: 'Allilla',
                },
                {
                  es: 'Mas o menos',
                  kichua: 'Shina Shinalla',
                },
                {
                  es: 'Mal',
                  kichua: 'Mana Allichu',
                },
                {
                  es: 'No estoy bien',
                  kichua: 'Mana allichu kani',
                },
                {
                  es: '¿Dónde vives?',
                  kichua: 'Maipi kawsanwi',
                },
                {
                  es: 'Yo vivo en',
                  kichua: 'Ñukaka ……. kawsani',
                },
              ]
            },
            {
              numero: 2,
              nombre: 'PRONOMBRES PERSONALES',
              diccionario: [
                {
                  es: 'Yo',
                  kichua: 'Ñuca',
                },
                {
                  es: 'Tú',
                  kichua: 'Can',
                },
                {
                  es: 'Él',
                  kichua: 'Pai',
                },
                {
                  es: 'Ella',
                  kichua: 'Pai',
                },
                {
                  es: 'Nosotros',
                  kichua: 'Ñucanchij',
                },
                {
                  es: 'Ellos',
                  kichua: 'Paicuna',
                },
              ]
            },
            {
              numero: 3,
              nombre: 'ARTÍCULOS',
              diccionario: [
                {
                  es: 'El',
                  kichua: 'Chai',
                },
                {
                  es: 'La',
                  kichua: 'Chai',
                },
                {
                  es: 'Las',
                  kichua: 'Chai al final usar la palabra Cuna',
                },
                {
                  es: 'Los',
                  kichua: 'Chai al final usar la palabra Cuna',
                },
                {
                  es: 'Un',
                  kichua: 'Shuj',
                },
                {
                  es: 'Una',
                  kichua: 'Shuj ',
                },
                {
                  es: 'Unos',
                  kichua: 'Achca',
                },
                {
                  es: 'Unas',
                  kichua: 'Achca',
                },
              ]
            }
          ],
          evaluacion: [
            {
              numero: 1,
              texto: 'Alli Buenos días',
              tipo: 'Completación',
              respuestaCorrecta: 'Buenos días',
              salto: 0,
              nota: 2
            },
            {
              numero: 2,
              texto: 'Mi nombre es David shuti David mikan.',
              tipo: 'Completación',
              respuestaCorrecta: 'Mi nombre es David',
              salto: 0,
              nota: 2
            },
            {
              numero: 3,
              texto: 'La flor sisa.',
              tipo: 'Completación',
              respuestaCorrecta: 'La flor',
              salto: 0,
              nota: 2
            },
            {
              numero: 4,
              texto: 'El pan tanda.',
              tipo: 'Completación',
              respuestaCorrecta: 'El pan',
              salto: 0,
              nota: 2
            },
            {
              numero: 5,
              texto: 'Imanalla ¿Cómo estás?',
              tipo: 'Completación',
              respuestaCorrecta: 'Cómo estás',
              salto: 0,
              nota: 2
            },
            {
              numero: 6,
              texto: 'Alli punlla significa buenas tardes.',
              tipo: 'Cuestionario',
              respuestaCorrecta: 'true',
              salto: 0,
              nota: 2
            },
            {
              numero: 7,
              texto: 'Maipi kawsanwi significa ¿Dónde vives?',
              tipo: 'Cuestionario',
              respuestaCorrecta: 'true',
              salto: 0,
              nota: 2
            },
            {
              numero: 8,
              texto: 'Yo vivo en quito se dice: Ñukaka Quito kawsani.',
              tipo: 'Cuestionario',
              respuestaCorrecta: 'true',
              salto: 0,
              nota: 2
            },
            {
              numero: 9,
              texto: 'Nosotros se dice Paicuna.',
              tipo: 'Cuestionario',
              respuestaCorrecta: 'true',
              salto: 0,
              nota: 2
            },
            {
              numero: 10,
              texto: 'Cuando nos preguntan ¿Ima shuti kanki?, debemos responder: Ñuka shutimi kan. ',
              tipo: 'Cuestionario',
              respuestaCorrecta: 'true',
              salto: 0,
              nota: 2
            }
          ]
        }
      ).save(),

      new Leccion(
        {
          numero: 2,
          nombre: 'Sustantivos, Colores etc',
          recursos: [
            {
              name: '1. Adjetivos Demostrativos',
              url: '1. Adjetivos Demostrativos.pdf'
            },
            {
              name: '2. Pronombres Posesivos',
              url: '2. Pronombres Posesivos.pdf'
            },
            {
              name: '3. Vocabulario Familia',
              url: '3. Vocabulario Familia.pdf'
            },
            {
              name: '4. COLORES',
              url: '4. COLORES.pdf'
            },
            {
              name: '5. Sustantivos',
              url: '5. Sustantivos.pdf'
            }
          ],
          actividad: `
              • Adjetivos Demostrativos
              • Pronombres Posesivos
              • Vocabulario Familia
              • Colores
              • Sustantivos

              1. Actividad de la lección 1

          Objetivos:
          En esta unidad aprenderá como usar los adjetivos demostrativos y los pronombres posesivos al hacer oraciones. 
          El estudiante logrará reconocer el vocabulario correspondiente a la familia, los color
              • Adjetivos Demostrativos
              • Pronombres Posesivos
              • Vocabulario Familia
              • Colores
              • Sustantivos

            2. Actividad de la lección 1

            Objetivos:es y algunos de los sustantivos más utilizados.
            
            Actividad:
                d. Revisar el contenido
                e. Apoyarse en los recursos para complementar el aprendizaje.
                f. Al finalizar, deberá responder el cuestionario para practicar lo aprendido.`,
          niveles: [
            {
              numero: 1,
              nombre: 'ADJETIVOS DEMOSTRATIVOS',
              diccionario: [
                {
                  es: 'Esta/Este/Esto',
                  kichua: 'Kay',
                },
                {
                  es: 'Esa/Ese/Eso',
                  kichua: 'Chay',
                },
                {
                  es: 'Estas/Estos',
                  kichua: 'Kaykuna',
                },
                {
                  es: 'Esas/Esos',
                  kichua: 'Chaykuna',
                },
              ]
            },
            {
              numero: 2,
              nombre: 'PRONOMBRES POSESIVOS',
              diccionario: [
                {
                  es: 'Mi',
                  kichua: 'Ñukapa',
                },
                {
                  es: 'Tú (Singular Familiar)',
                  kichua: 'Kanpa',
                },
                {
                  es: 'Su (Singular Formal)',
                  kichua: 'Kikinpa',
                },
                {
                  es: 'Su',
                  kichua: 'Paypa',
                },
                {
                  es: 'Nuestro',
                  kichua: 'Ñukanchikpa',
                },
                {
                  es: 'De ustedes (plural familiar)',
                  kichua: 'Kankunapa',
                },
                {
                  es: 'De ustedes (plural formal)',
                  kichua: 'Kikinkunapa',
                },
                {
                  es: 'De ellos',
                  kichua: 'Paykunapa',
                },
              ]
            },

            {
              numero: 3,
              nombre: 'VOCABULARIO FAMILIA',
              diccionario: [
                {
                  es: 'Papá',
                  kichua: 'Yaya',
                },
                {
                  es: 'Mamá',
                  kichua: 'Mama',
                },
                {
                  es: 'Padres',
                  kichua: 'Taytamamakuna',
                },
                {
                  es: 'Esposo',
                  kichua: 'Cusa',
                },
                {
                  es: 'Esposa/mujer',
                  kichua: 'Warmi',
                },
                {
                  es: 'Hombre',
                  kichua: 'Jari',
                },
                {
                  es: 'Niño',
                  kichua: 'Wahua',
                },
                {
                  es: 'Amigo',
                  kichua: 'Mashi',
                },
                {
                  es: 'Familia',
                  kichua: 'Familia',
                },
                {
                  es: 'Hijo',
                  kichua: 'Churi',
                },
                {
                  es: 'Hija',
                  kichua: 'Ushushi',
                },
                {
                  es: 'Abuelo',
                  kichua: 'Hatun yaya',
                },
                {
                  es: 'Abuela',
                  kichua: 'Hatun Mama',
                },
                {
                  es: 'Nuera',
                  kichua: 'Kachun',
                },
              ]
            },
            {
              numero: 4,
              nombre: 'COLORES',
              diccionario: [
                {
                  es: 'Blanco',
                  kichua: 'Yuraj',
                },
                {
                  es: 'Rojo',
                  kichua: 'Puca',
                },
                {
                  es: 'Amarillo',
                  kichua: 'Killu',
                },
                {
                  es: 'Amarillo Claro',
                  kichua: 'Chawa Killu',
                },
                {
                  es: 'Negro',
                  kichua: 'Yana',
                },
                {
                  es: 'Rosado',
                  kichua: 'Waminisi',
                },
                {
                  es: 'Azul',
                  kichua: 'Ankas',
                },
                {
                  es: 'Azul marino',
                  kichua: 'Yanalla Ankas',
                },
                {
                  es: 'Celeste',
                  kichua: 'Chawa Ankas',
                },
                {
                  es: 'Verde',
                  kichua: 'Waylla',
                },
                {
                  es: 'Verde Claro',
                  kichua: 'Chawa Waylla',
                },
                {
                  es: 'Naranja',
                  kichua: 'Kishpu',
                },
                {
                  es: 'Café ',
                  kichua: 'Paku',
                },
                {
                  es: 'Morado',
                  kichua: 'Maywa',
                },
                {
                  es: 'Plomo',
                  kichua: 'Suku',
                },
              ]
            },
            {
              numero: 5,
              nombre: 'SUSTANTIVOS',
              diccionario: [
                {
                  es: 'Justicia',
                  kichua: 'Cashcata rurana',
                },
                {
                  es: 'Fe',
                  kichua: 'Crina',
                },
                {
                  es: 'Verdad',
                  kichua: 'Ama llullushpa',
                },
                {
                  es: 'Hambre',
                  kichua: 'Yaricay',
                },
                {
                  es: 'Pobreza',
                  kichua: 'Huajcha',
                },
                {
                  es: 'Riqueza',
                  kichua: 'Charij',
                },
                {
                  es: 'Dulce',
                  kichua: 'Mishki',
                },
                {
                  es: 'Guerra ',
                  kichua: 'Macanacuna',
                },
                {
                  es: 'Sueño',
                  kichua: 'Muscuy',
                },
                {
                  es: 'Miedo',
                  kichua: 'Manchay',
                },
                {
                  es: 'Terror',
                  kichua: 'Manchay',
                },
                {
                  es: 'Inteligencia',
                  kichua: 'Yachaysapa',
                },
                {
                  es: 'Pensamiento',
                  kichua: 'Yuyay',
                },
                {
                  es: 'Amor',
                  kichua: 'C´uyay',
                },
                {
                  es: 'Dolor',
                  kichua: 'Nanay',
                },
                {
                  es: 'Mentira',
                  kichua: 'Llulla',
                },
                {
                  es: 'Contento',
                  kichua: 'Cushilla',
                },
                {
                  es: 'País, región, pueblo ',
                  kichua: 'Llacta',
                },
                {
                  es: 'Nombre',
                  kichua: 'Shuti',
                },
                {
                  es: 'Problema/tristeza',
                  kichua: 'llaki',
                },
                {
                  es: 'Valentía',
                  kichua: 'Valishca',
                },
                {
                  es: 'Muerte',
                  kichua: 'Wañuy',
                },
                {
                  es: 'Hambre',
                  kichua: 'Yarikay',
                },
                {
                  es: 'Pelea/Guerra',
                  kichua: 'Makanakuy',
                },

              ]
            },
          ],
          evaluacion: [
            {
              numero: 1,
              texto: 'Ñuka  Mi papá.',
              tipo: 'Completación',
              respuestaCorrecta: 'Mi papá',
              salto: 0,
              nota: 2
            },
            {
              numero: 2,
              texto: 'Tú esposo cusa.',
              tipo: 'Completación',
              respuestaCorrecta: 'Tú esposo',
              salto: 0,
              nota: 2
            },
            {
              numero: 3,
              texto: 'Paypa Su hijo',
              tipo: 'Completación',
              respuestaCorrecta: 'Su hijo',
              salto: 0,
              nota: 2
            },
            {
              numero: 4,
              texto: 'Sisa Flor Blanco.',
              tipo: 'Completación',
              respuestaCorrecta: 'Flor Blanco',
              salto: 0,
              nota: 2
            },
            {
              numero: 5,
              texto: 'Allku Perro Negro',
              tipo: 'Completación',
              respuestaCorrecta: 'Perro Negro',
              salto: 0,
              nota: 2
            },
            {
              numero: 6,
              texto: 'Ñuka  Mi papá.',
              tipo: 'Cuestionario',
              respuestaCorrecta: 'true',
              salto: 0,
              nota: 2
            },
            {
              numero: 7,
              texto: 'Tú esposo cusa.',
              tipo: 'Cuestionario',
              respuestaCorrecta: 'true',
              salto: 0,
              nota: 2
            },
            {
              numero: 8,
              texto: 'Paypa Su hijo',
              tipo: 'Cuestionario',
              respuestaCorrecta: 'true',
              salto: 0,
              nota: 2
            },
            {
              numero: 9,
              texto: 'Sisa Flor Blanco.',
              tipo: 'Cuestionario',
              respuestaCorrecta: 'true',
              salto: 0,
              nota: 2
            },
            {
              numero: 10,
              texto: 'Allku Perro Negro',
              tipo: 'Cuestionario',
              respuestaCorrecta: 'true',
              salto: 0,
              nota: 2
            }
          ]
        }
      ).save(),

      new Leccion(
        {
          numero: 3,
          nombre: 'Números, Animales etc',
          recursos: [
            {
              name: '1. ANIMALES',
              url: '1. ANIMALES.pdf'
            },
            {
              name: '2. PARTES DEL CUERPO HUMANO',
              url: '2. PARTES DEL CUERPO HUMANO.pdf'
            },
            {
              name: '3. NÚMEROS',
              url: '3. NÚMEROS.pdf'
            }
          ],
          actividad: `    1. Actividad de la lección 3

          Objetivos:
          En esta unidad aprenderá a reconocer los números del 1 al 10 000, también podrá identificar las partes del cuerpo humano y conocer los nombres de los animales.
          
          Actividad:
              a. Revisar el contenido
              b. Apoyarse en los recursos para complementar el aprendizaje.
              c. Al finalizar, deberá responder el cuestionario para practicar lo aprendido.`,
          niveles: [
            {
              numero: 1,
              nombre: 'NÚMEROS',
              diccionario: [
                {
                  es: '1',
                  kichua: 'Shuk',
                },
                {
                  es: '2',
                  kichua: 'Ishkay',
                },
                {
                  es: '3',
                  kichua: 'Kimsa',
                },
                {
                  es: '4',
                  kichua: 'Chusku',
                },
                {
                  es: '5',
                  kichua: 'Pishka',
                },
                {
                  es: '6',
                  kichua: 'Sukta',
                },
                {
                  es: '7',
                  kichua: 'Canchis',
                },
                {
                  es: '8',
                  kichua: 'Pusak',
                },
                {
                  es: '9',
                  kichua: 'Iskun',
                },
                {
                  es: '10',
                  kichua: 'Chunka',
                },
                {
                  es: '11',
                  kichua: 'Chunka Shuk',
                },
                {
                  es: '12',
                  kichua: 'Chunka Ishkay',
                },
                {
                  es: '13',
                  kichua: 'Chunka Kimsa',
                },
                {
                  es: '14',
                  kichua: 'Chunka Chusku',
                },
                {
                  es: '15',
                  kichua: 'Chunka Pishka',
                },
                {
                  es: '16',
                  kichua: 'Chunka Sukta',
                },
                {
                  es: '17',
                  kichua: 'Chunka Canchis',
                },
                {
                  es: '18',
                  kichua: 'Chunka Pusak',
                },
                {
                  es: '19',
                  kichua: 'Chunka Iskun',
                },
                {
                  es: '20',
                  kichua: 'Ishkay Chunka',
                },
                {
                  es: '30',
                  kichua: 'Kimsa Chunka',
                },
                {
                  es: '40',
                  kichua: 'Chusku Chunka',
                },
                {
                  es: '50',
                  kichua: 'Pichka Chunka',
                },
                {
                  es: '60',
                  kichua: 'Sukta Chunka',
                },
                {
                  es: '70',
                  kichua: 'Canchis Chunka',
                },
                {
                  es: '80',
                  kichua: 'Pusak Chunka',
                },
                {
                  es: '90',
                  kichua: 'Iskun Chunka',
                },
                {
                  es: '100',
                  kichua: 'Patsak',
                },
                {
                  es: '1000',
                  kichua: 'Waranka',
                },
                {
                  es: '1000000',
                  kichua: 'Hunu',
                },

              ]
            },
            {
              numero: 2,
              nombre: 'PARTES DEL CUERPO HUMANO',
              diccionario: [
                {
                  es: 'Cabeza',
                  kichua: 'Uma',
                },
                {
                  es: 'Cabello',
                  kichua: 'Ajcha',
                },
                {
                  es: 'Ojo',
                  kichua: 'Ñawi',
                },
                {
                  es: 'Nariz',
                  kichua: 'Singa',
                },
                {
                  es: 'Oreja',
                  kichua: 'Rinri',
                },
                {
                  es: 'Boca',
                  kichua: 'Shimi',
                },
                {
                  es: 'Cuello',
                  kichua: 'Cunga',
                },
                {
                  es: 'Hombros',
                  kichua: 'Callmi',
                },
                {
                  es: 'Dientes',
                  kichua: 'Quiru',
                },
                {
                  es: 'Corazón',
                  kichua: 'Shungo',
                },
                {
                  es: 'Barriga',
                  kichua: 'Wiksa',
                },
                {
                  es: 'Brazo',
                  kichua: 'Rigra',
                },
                {
                  es: 'Mano',
                  kichua: 'Maki',
                },
                {
                  es: 'Rodilla',
                  kichua: 'Sampi',
                },
                {
                  es: 'Dedos',
                  kichua: 'Dedocuna',
                },
                {
                  es: 'Pie',
                  kichua: 'Chaki',
                },
              ]
            },
            {
              numero: 3,
              nombre: 'ANIMALES',
              diccionario: [
                {
                  es: 'Perro',
                  kichua: 'Ashcu',
                },
                {
                  es: 'Gato',
                  kichua: 'Misi',
                },
                {
                  es: 'Vaca',
                  kichua: 'Wagra',
                },
                {
                  es: 'Chivo',
                  kichua: 'Chita',
                },
                {
                  es: 'Rata',
                  kichua: 'Piricu',
                },
                {
                  es: 'Pájaro ',
                  kichua: 'Muntie',
                },
                {
                  es: 'Caballo ',
                  kichua: 'Apyu/Uyhua (hembra)',
                },
                {
                  es: 'Gallina',
                  kichua: 'Atallpa',
                },
                {
                  es: 'Pato',
                  kichua: 'Kulta',
                },
                {
                  es: 'Cuy',
                  kichua: 'Kuy',
                },
                {
                  es: 'Chancho',
                  kichua: 'Kuchi',
                },
                {
                  es: 'Pulga',
                  kichua: 'Piki',
                },
                {
                  es: 'Conejo',
                  kichua: 'Wallinku',
                },
                {
                  es: 'Lobo',
                  kichua: 'Atuk',
                },
                {
                  es: 'Mono',
                  kichua: 'Kushillu',
                },
                {
                  es: 'Culebra',
                  kichua: 'Amaru',
                },
                {
                  es: 'Sapo',
                  kichua: 'Hampatu',
                },
                {
                  es: 'Llama',
                  kichua: 'Runa llama',
                },
                {
                  es: 'Aguila',
                  kichua: 'Anka',
                },
                {
                  es: 'Borrego',
                  kichua: 'Wiwika',
                },
                {
                  es: 'Ratón',
                  kichua: 'Ukucha',
                },
                {
                  es: 'Mosca, insecto',
                  kichua: 'Chuspi',
                },
                {
                  es: 'Pez',
                  kichua: 'Chalwa',
                },

              ]
            },
          ],
          evaluacion: [
            {
              numero: 1,
              texto: 'Shina Shinalla significa Mas o menos',
              tipo: 'Cuestionario',
              respuestaCorrecta: 'true',
              salto: 0,
              nota: 10
            },
            {
              numero: 2,
              texto: 'Ñukata payta rikunkapakmi chayman rini',
              tipo: 'Completación',

              respuestaCorrecta: 'rikunkapakmi',
              salto: 0,
              nota: 10
            }
          ]
        }
      ).save(),

      new Leccion(
        {
          numero: 4,
          nombre: 'Cortesías, Despedidas etc',
          recursos: [
            {
              name: '1. CORTESIAS',
              url: '1. CORTESIAS.pdf'
            },
            {
              name: '2. DESPEDIDAS',
              url: '2. DESPEDIDAS.pdf'
            },
            {
              name: '3. ALIMENTOS',
              url: '3. ALIMENTOS.pdf'
            }
          ],
          actividad: `
                • Cortesías
                • Despedidas 
                • Alimentos
            
                a) Actividad de la lección 4
            
            Objetivos:
            En esta unidad aprenderá como expresarse con cortesía, además conocerá como despedirse correctamente. 
            El estudiante logrará reconocer los alimentos más utilizados en el idioma Kichwa.`,
          niveles: [
            {
              numero: 1,
              nombre: 'CORTESÍAS',
              diccionario: [
                {
                  es: 'Buenísimo',
                  kichua: 'Allimi',
                },
                {
                  es: 'Por favor',
                  kichua: 'Ama shina Kashpa',
                },
                {
                  es: 'Escuche por favor',
                  kichua: 'kai favortaca uyay',
                },
                {
                  es: 'Podemos pasar',
                  kichua: 'Pasay valinchijchu',
                },
                {
                  es: 'Gracias',
                  kichua: 'Pagui',
                },
                {
                  es: 'Invíteme a entrar',
                  kichua: '¡Mingachiway!',
                },
                {
                  es: 'Siga adelante, entre por favor.',
                  kichua: '¡Mingaripay!',
                },
                {
                  es: 'Esta pasando el día',
                  kichua: 'Ñami punlla pasacun',
                },
              ],
            },
            {
              numero: 2,
              nombre: 'DESPEDIDAS',
              diccionario: [
                {
                  es: 'Hasta mañana',
                  kichua: 'Kayakama',
                },
                {
                  es: 'Nos vemos otro día',
                  kichua: 'Shuktaj punlla rikunakuchun',
                },
                {
                  es: 'Adiós',
                  kichua: 'Tupashun',
                },
                {
                  es: 'Hasta la tarde',
                  kichua: 'Chishi caman',
                },
                {
                  es: 'Que tenga un buen dia',
                  kichua: 'Shuj allí punllata charingui',
                },
                {
                  es: 'Hasta pronto',
                  kichua: 'Shuctak rato kama',
                },
                {
                  es: 'Hasta luego',
                  kichua: 'Ashta kashkama',
                },
                {
                  es: 'Nos toparemos',
                  kichua: 'Tuparishun',
                },
              ],
            },
            {
              numero: 3,
              nombre: 'ALIMENTOS',
              diccionario: [
                {
                  es: 'Carne',
                  kichua: 'Aicha',
                },
                {
                  es: 'Sal',
                  kichua: 'Cachi',
                },
                {
                  es: 'Harina',
                  kichua: 'Polbu/Haku',
                },
                {
                  es: 'Frejol',
                  kichua: 'Purutu',
                },
                {
                  es: 'Queso',
                  kichua: 'Makinchu',
                },
                {
                  es: 'Azúcar',
                  kichua: 'Mishki Haku',
                },
                {
                  es: 'Pan',
                  kichua: 'Tanta',
                },
                {
                  es: 'Aceite',
                  kichua: 'Yaku Wira',
                },
                {
                  es: 'Agua',
                  kichua: 'Yahu',
                },
                {
                  es: 'Leche',
                  kichua: 'Ñuñu',
                },
                {
                  es: 'Pollo',
                  kichua: 'Chuchi aycha',
                },
                {
                  es: 'Pescado',
                  kichua: 'Challwa',
                },
                {
                  es: 'Aguacate',
                  kichua: 'Palta',
                },
                {
                  es: 'Maíz',
                  kichua: 'Sara',
                },
                {
                  es: 'Yuca',
                  kichua: 'Lumu',
                },
                {
                  es: 'Quinua',
                  kichua: 'Kinua',
                },
                {
                  es: 'Calabaza',
                  kichua: 'Zampu',
                },
                {
                  es: 'Zanahoria',
                  kichua: 'Rakacha',
                },
                {
                  es: 'Ají',
                  kichua: 'Uchú',
                },
                {
                  es: 'Naranja',
                  kichua: 'Chilina',
                },
                {
                  es: 'Piña',
                  kichua: 'Chiwila',
                },
                {
                  es: 'Melloco',
                  kichua: 'Milluku',
                },
                {
                  es: 'Papa',
                  kichua: 'Papa',
                },
              ],
            },
          ],
          evaluacion: [
            {
              numero: 1,
              texto: 'Shina Shinalla significa Mas o menos',
              tipo: 'Cuestionario',
              respuestaCorrecta: 'true',
              salto: 0,
              nota: 10
            },
            {
              numero: 2,
              texto: 'Ñukata payta rikunkapakmi chayman rini',
              tipo: 'Completación',

              respuestaCorrecta: 'rikunkapakmi',
              salto: 0,
              nota: 10
            }
          ]
        }
      ).save(),

      new Leccion(
        {
          numero: 5,
          nombre: 'COSAS DE LA CASA',
          recursos: [
            {
              name: '1. COSAS DE LA CASA',
              url: '1. COSAS DE LA CASA.pdf'
            },
            {
              name: '2. PARTES DE LA CASA',
              url: '2. PARTES DE LA CASA.pdf'
            },
            {
              name: '3. COSAS DEL AULA',
              url: '3. COSAS DEL AULA.pdf'
            }
          ],
          actividad: `
                • Cosas de la casa
                • Partes de la casa
                • Cosas del Aula
            
                a) Actividad de la lección 5
            
            Objetivos:
            En esta unidad aprenderá el vocabulario respectivo sobre las cosas de la casa y del aula.
            El estudiante logrará reconocer las partes de la casa y sus respectivos nombres.
            
            Actividad:
                a. Revisar el contenido
                b. Apoyarse en los recursos para complementar el aprendizaje.
                c. Al finalizar, deberá responder el cuestionario para practicar lo aprendido.`,
          niveles: [
            {
              numero: 1,
              nombre: 'COSAS DE LA CASA',
              diccionario: [
                {
                  es: 'Casa',
                  kichua: 'Wasi',
                },
                {
                  es: 'Suelo',
                  kichua: 'Pamba',
                },
                {
                  es: 'Pared',
                  kichua: 'Pirka',
                },
                {
                  es: 'Puerta',
                  kichua: 'Punku',
                },
                {
                  es: 'Ventana',
                  kichua: 'Tuku',
                },
                {
                  es: 'Leña',
                  kichua: 'Yanta',
                },
                {
                  es: 'Fuego',
                  kichua: 'Nina',
                },
                {
                  es: 'Fosforo',
                  kichua: 'Pakuyla',
                },
                {
                  es: 'Mesa',
                  kichua: 'Pataku',
                },
                {
                  es: 'Silla',
                  kichua: 'Tiyarina',
                },
                {
                  es: 'Cucharon',
                  kichua: 'Mama wisha',
                },
                {
                  es: 'Cuchara',
                  kichua: 'wisha',
                },
                {
                  es: 'Olla',
                  kichua: 'Manka',
                },
                {
                  es: 'Almohada',
                  kichua: 'Kawitu',
                },
                {
                  es: 'Cama',
                  kichua: 'Sawna',
                },
                {
                  es: 'Cobija',
                  kichua: 'Katana',
                },
                {
                  es: 'Ducha',
                  kichua: 'Armana Uku',
                },

              ],
            },
            {
              numero: 2,
              nombre: 'PARTES DE LA CASA',
              diccionario: [
                {
                  es: 'Cocina',
                  kichua: 'Yanuna Uku',
                },
                {
                  es: 'Baño',
                  kichua: 'Ishpana Uku',
                },
                {
                  es: 'Dormitorio',
                  kichua: 'Puñuna Uku',
                },
              ],
            },
            {
              numero: 3,
              nombre: 'COSAS DEL AULA',
              diccionario: [
                {
                  es: 'Profesor   ',
                  kichua: 'Yachachijk',
                },
                {
                  es: 'Estudiante',
                  kichua: 'Yachakuk',
                },
                {
                  es: 'Pupitre',
                  kichua: 'Banco tiyarina',
                },
                {
                  es: 'Libro ',
                  kichua: 'Kamu',
                },
                {
                  es: 'Silla',
                  kichua: 'Tiyarina',
                },
                {
                  es: 'Mesa ',
                  kichua: 'Pataku',
                },
                {
                  es: 'Bandera',
                  kichua: 'Wipala',
                },
                {
                  es: 'Pizarrón',
                  kichua: 'Killkana Pirka',
                },
                {
                  es: 'Lápiz',
                  kichua: 'Killkana Kaspi',
                },
              ],
            },
          ],
          evaluacion: [
            {
              numero: 1,
              texto: 'Shina Shinalla significa Mas o menos',
              tipo: 'Cuestionario',
              respuestaCorrecta: 'true',
              salto: 0,
              nota: 10
            },
            {
              numero: 2,
              texto: 'Ñukata payta rikunkapakmi chayman rini',
              tipo: 'Completación',

              respuestaCorrecta: 'rikunkapakmi',
              salto: 0,
              nota: 10
            }
          ]
        }
      ).save(),

      new Leccion(
        {
          numero: 6,
          nombre: 'Cosas de la Naturaleza',
          recursos: [
            {
              name: '1. COSAS DE LA NATURALEZA',
              url: '1. COSAS DE LA NATURALEZA.pdf'
            },
            {
              name: '2. Clima',
              url: '2. Clima.pdf'
            },
            {
              name: '3. PLANTAS Y VEGETALES',
              url: '3. PLANTAS Y VEGETALES.pdf'
            },
            {
              name: 'ADJETIVOS',
              url: 'ADJETIVOS.pdf'
            }
          ],
          actividad: `
                • Cosas de la Naturaleza
                • Clima
                • Planta y Vegetales
                • Adjetivos
            
                a) Actividad de la lección 6
            
            Objetivos:
            En esta unidad conocerá algunas palabras para que pueda aprender como se dice algunas cosas de la naturaleza en Kichwa.
            El estudiante comenzara aprendiendo algunos adjetivos que se usan en el diario vivir.
            
            Actividad:
                a. Revisar el contenido
                b. Apoyarse en los recursos para complementar el aprendizaje.
                c. Al finalizar, deberá responder el cuestionario para practicar lo aprendido.`,
          actividad: 'Actividad 6 <br> Test Actividad lorem ipsum fija erc...',
          niveles: [
            {
              numero: 1,
              nombre: 'COSAS DE LA NATURALEZA',
              diccionario: [
                {
                  es: 'Nube',
                  kichua: 'Puyu',
                },
                {
                  es: 'Nevado',
                  kichua: 'Rasu',
                },
                {
                  es: 'Rio',
                  kichua: 'Mayu',
                },
                {
                  es: 'Arco iris',
                  kichua: 'Kuychi',
                },
                {
                  es: 'Laguna',
                  kichua: 'Hatun kucha',
                },
                {
                  es: 'Camino',
                  kichua: 'Ñan',
                },
                {
                  es: 'Cascada',
                  kichua: 'Pakcha',
                },
                {
                  es: 'Piedra',
                  kichua: 'Rumi',
                },
                {
                  es: 'Montaña',
                  kichua: 'Urku',
                },
                {
                  es: 'Rayo',
                  kichua: 'Illapa',
                },
                {
                  es: 'Corral',
                  kichua: 'Kincha',
                },
                {
                  es: 'Selva',
                  kichua: 'Sacha',
                },
                {
                  es: 'Lodo',
                  kichua: 'Turu',
                },
                {
                  es: 'Paja ',
                  kichua: 'uksha',
                },
                {
                  es: 'Fuego ',
                  kichua: 'Nina ',
                },
                {
                  es: 'Aire ',
                  kichua: 'Wayra ',
                },
                {
                  es: 'Hierba',
                  kichua: 'Kiwa',
                },
                {
                  es: 'Valle',
                  kichua: 'Pampa',
                },
                {
                  es: 'Lluvia',
                  kichua: 'Tamya',
                },
                {
                  es: 'Amanecer',
                  kichua: 'Pacaricun',
                },
                {
                  es: 'Sol',
                  kichua: 'Inti',
                },

              ]
            },
            {
              numero: 2,
              nombre: 'CLIMA',
              diccionario: [
                {
                  es: '¿Hace buen tiempo?',
                  kichua: '¿Alli tiempochu?',
                },
                {
                  es: 'Hace buen tiempo',
                  kichua: 'Alli tiempomi',
                },
                {
                  es: 'Hace mal tiempo',
                  kichua: 'Mana allí tiempochu',
                },
                {
                  es: 'Hace sol',
                  kichua: 'Intikunmi',
                },
                {
                  es: 'Hace frio',
                  kichua: 'Chiirimi',
                },
                {
                  es: 'Hace calor',
                  kichua: 'Cunucmi',
                },
                {
                  es: 'Hace viento',
                  kichua: 'Wayrami',
                },
                {
                  es: 'Esta lloviendo',
                  kichua: 'Tamyacunmi',
                },
                {
                  es: 'Esta granizando',
                  kichua: 'Runtucunmi',
                },
                {
                  es: 'Esta Nevando',
                  kichua: 'Rasucunmi',
                },

              ]
            },
            {
              numero: 3,
              nombre: 'PLANTAS Y VEGETALES',
              diccionario: [
                {
                  es: 'Ortiga',
                  kichua: 'Chini',
                },
                {
                  es: 'Flor',
                  kichua: 'Sisa',
                },
                {
                  es: 'Pasto',
                  kichua: 'Kiwa',
                },
                {
                  es: 'Madera',
                  kichua: 'Kaspi',
                },
                {
                  es: 'Clavel',
                  kichua: 'Wayta',
                },
                {
                  es: 'Árbol ',
                  kichua: 'Yura, kiru',
                },
                {
                  es: 'Penco',
                  kichua: 'Chawar',
                },
                {
                  es: 'Raiz',
                  kichua: 'Sapi',
                },

              ]
            },
            {
              numero: 4,
              nombre: 'ADJETIVOS',
              diccionario: [
                {
                  es: 'Pequeño',
                  kichua: 'Uchilla',
                },
                {
                  es: 'Grande',
                  kichua: 'Jatun',
                },
                {
                  es: 'Alto',
                  kichua: 'Jatun ',
                },
                {
                  es: 'Delgado',
                  kichua: 'Chuso',
                },
                {
                  es: 'Gordo',
                  kichua: 'Huira',
                },
                {
                  es: 'Bravo',
                  kichua: 'Fiña',
                },
                {
                  es: 'Enojado',
                  kichua: 'Fama',
                },
                {
                  es: 'Feliz  ',
                  kichua: 'Kushilla',
                },
                {
                  es: 'Simpático',
                  kichua: 'Alaja',
                },
                {
                  es: 'Triste  ',
                  kichua: 'Llakilla',
                },
              ]
            },
          ],
          evaluacion: [
            {
              numero: 1,
              texto: 'Shina Shinalla significa Mas o menos',
              tipo: 'Cuestionario',

              respuestaCorrecta: 'true',
              salto: 0,
              nota: 10
            },
            {
              numero: 2,
              texto: 'Ñukata payta rikunkapakmi chayman rini',
              tipo: 'Completación',

              respuestaCorrecta: 'rikunkapakmi',
              salto: 0,
              nota: 10
            }
          ]
        }
      ).save(),

      new Leccion(
        {
          numero: 7,
          nombre: 'Verbos más Utilizados',
          recursos: [
            {
              name: '1. VERBOS MÁS UTILIZADOS',
              url: '1. VERBOS MÁS UTILIZADOS.pdf'
            },
            {
              name: '2. TERMINACIÓN PARA LOS VERBOS SEGÚN LOS PRONOMBRES',
              url: '2. TERMINACIÓN PARA LOS VERBOS SEGÚN LOS PRONOMBRES.pdf'
            },
            {
              name: '3. CONJUGACIÓN DEL VERBO SER',
              url: '3. CONJUGACIÓN DEL VERBO SER.pdf'
            },
            {
              name: '4. ESTRUCTURA DE ORACIONES',
              url: '4. ESTRUCTURA DE ORACIONES.pdf'
            }
          ],
          actividad: `    
                • Verbos más utilizados
                • Terminaciones para los verbos en tiempo presente.
                • Conjugación del verbo ser en presente.
                • Estructura de oraciones
            
                a) Actividad de la lección 7
            
            Objetivos:
            En esta unidad aprenderá como se dicen los verbos más utilizados en Kichwa
            Además, El estudiante logrará entender cómo se realiza la conjugación de los verbos en tiempo presente.`,
          niveles: [
            {
              numero: 1,
              nombre: 'VERBOS MÁS UTILIZADOS',
              diccionario: [
                {
                  es: 'Hablar',
                  kichua: 'Parlana',
                },
                {
                  es: 'Enseñar',
                  kichua: 'Yachachina',
                },
                {
                  es: 'Cortar  ',
                  kichua: 'Cuchuna',
                },
                {
                  es: 'Cocinar',
                  kichua: 'Yanuna',
                },
                {
                  es: 'Sentarse',
                  kichua: 'Tiyarina',
                },
                {
                  es: 'Escribir',
                  kichua: 'Killkana/escribina',
                },
                {
                  es: 'Pensar',
                  kichua: 'Yuyana',
                },
                {
                  es: 'Tener',
                  kichua: 'Charina',
                },
                {
                  es: 'Vender',
                  kichua: 'Jatuna',
                },
                {
                  es: 'Comer',
                  kichua: 'Mikuna',
                },
                {
                  es: 'Mirar',
                  kichua: 'Rikuna',
                },
                {
                  es: 'Contar',
                  kichua: 'Yupana',
                },
                {
                  es: 'Morder',
                  kichua: 'Kanina',
                },
                {
                  es: 'Pintar',
                  kichua: 'Pintana',
                },
                {
                  es: 'Leer',
                  kichua: 'Liyina',
                },
                {
                  es: 'Aprender',
                  kichua: 'Yachana',
                },
                {
                  es: 'Amar',
                  kichua: 'Kuyana',
                },
                {
                  es: 'Creer  ',
                  kichua: 'Krina',
                },
                {
                  es: 'Seguir',
                  kichua: 'Katina',
                },
                {
                  es: 'Acercarse',
                  kichua: 'Kuchuyana',
                },
                {
                  es: 'Enviar',
                  kichua: 'Kachana',
                },
                {
                  es: 'Guiar',
                  kichua: 'Pushana',
                },
                {
                  es: 'Ser',
                  kichua: 'Kana',
                },
                {
                  es: 'Decir',
                  kichua: 'Nina',
                },
                {
                  es: 'Recordar',
                  kichua: 'Yuyarina',
                },
                {
                  es: 'Morir ',
                  kichua: 'Wañuna',
                },

              ]
            },
            {
              numero: 2,
              nombre: 'TERMINACIONES PARA LOS VERBOS EN TIEMPO PRESENTE',
              diccionario: [
                {
                  es: 'Ñuka',
                  kichua: '-ni',
                },
                {
                  es: 'Kan,Kikin',
                  kichua: '-nki',
                },
                {
                  es: 'Pay',
                  kichua: '-n',
                },
                {
                  es: 'Ñukanchik',
                  kichua: '-nchik',
                },
                {
                  es: 'Kankuna, Kikinkuna',
                  kichua: '-nkichik',
                },
                {
                  es: 'Paykuna',
                  kichua: '-nkuna/-n',
                },

              ]
            },
            {
              numero: 3,
              nombre: 'CONJUGACIÓN DEL VERBO SER EN PRESENTE',
              diccionario: [

                {
                  es: 'Yo soy',
                  kichua: 'Ñuka kani',
                },
                {
                  es: 'Tú eres',
                  kichua: 'Kan kanki',
                },
                {
                  es: 'Usted es',
                  kichua: 'Kikin kanki',
                },
                {
                  es: 'Él es, Ella es',
                  kichua: 'Pay kan',
                },
                {
                  es: 'Nosotros somos',
                  kichua: 'Ñukanchik kanchik',
                },
                {
                  es: 'Ustedes son (informal)',
                  kichua: 'Kankuna kankichik',
                },
                {
                  es: 'Ustedes son (informal)',
                  kichua: 'Kikinkuna Kankichik',
                },
                {
                  es: 'Ellos son',
                  kichua: 'Paykuna kan/ kankuna',
                },

              ]
            },
            {
              numero: 4,
              nombre: 'ESTRUCTURA DE ORACIONES',
              diccionario: [
                {
                  es: 'Mi nombre es Juan',
                  kichua: 'Ñuca  shuti  Juanmican',
                },
                {
                  es: 'Tú tienes una casa grande.',
                  kichua: 'Kanka jatun wasuyujmi cangui   ',
                },
                {
                  es: 'Él vive en Galápagos',
                  kichua: 'Paica Galapagospimi causan',
                },
                {
                  es: 'Nosotros somos 4 hijos.              ',
                  kichua: 'Ñucanchikca  chushcu churicunami canchik',
                },
                {
                  es: 'Ustedes son profesores.',
                  kichua: 'Cancunaca  yachachijcunami canguichij',
                },
                {
                  es: 'Ellos tienen un perro.  ',
                  kichua: 'Paicunaca  shuj ashcuyujmi',
                },
                {
                  es: 'El alumno escribe con el lápiz.      ',
                  kichua: 'Yachajca lapizhuanmi quishcacun',
                },
                {
                  es: 'El chico esta aprendiendo.',
                  kichua: 'Chai wambraca yachacunmi',
                },
                {
                  es: 'El profesor enseña.',
                  kichua: 'Paica Yachachinmi',
                },
                {
                  es: 'Los alumnos se sientan.',
                  kichua: 'Chai  Yachajcunaca tiyarinmi',
                },

              ]
            },
          ],
          evaluacion: [
            {
              numero: 1,
              texto: 'Shina Shinalla significa Mas o menos',
              tipo: 'Cuestionario',

              respuestaCorrecta: 'true',
              salto: 0,
              nota: 10
            },
            {
              numero: 2,
              texto: 'Ñukata payta rikunkapakmi chayman rini',
              tipo: 'Completación',

              respuestaCorrecta: 'rikunkapakmi',
              salto: 0,
              nota: 10
            }
          ]
        }
      ).save(),

      new Leccion(
        {
          numero: 8,
          nombre: 'Adjetivos, Antónimos, etc',
          recursos: [
            {
              name: '1. ADJETIVOS',
              url: '1. ADJETIVOS.pdf'
            },
            {
              name: '2. ANTÓNIMOS',
              url: '2. ANTÓNIMOS.pdf'
            },
            {
              name: '3. VERBOS',
              url: '3. VERBOS.pdf'
            },
            {
              name: '4. EXPRESIONES ÚTILES',
              url: '4. EXPRESIONES ÚTILES.pdf'
            }
          ],
          actividad: `
              • Adjetivos 
              • Antónimos 
              • Verbos
              • Expresiones útiles
          
              a) Actividad de la lección 8
          
          Objetivos:
          En esta unidad aprenderá de los Antónimos más comunes en el idioma de Kichwa
          Podrá aumentar su conocimiento en cuanto a los verbos y también aprenderá como se dicen varias expresiones útiles en Kichwa.
          
          Actividad:
              a. Revisar el contenido
              b. Apoyarse en los recursos para complementar el aprendizaje.
              c. Al finalizar, deberá responder el cuestionario para practicar lo aprendido.`,
          niveles: [
            {
              numero: 1,
              nombre: 'ADJETIVOS',
              diccionario: [
                {
                  es: 'Dulce  ',
                  kichua: 'Mishki',
                },
                {
                  es: 'Amargo',
                  kichua: 'Jayak',
                },
                {
                  es: 'Bonito, hermoso, magnifico',
                  kichua: 'Sumak/Kuyaylla/Alhaja',
                },
                {
                  es: 'Feo',
                  kichua: 'Mana Sumak',
                },
                {
                  es: 'Bueno',
                  kichua: 'Alli',
                },
                {
                  es: 'Malo',
                  kichua: 'Mana Alli/nalli',
                },
                {
                  es: 'Nuevo',
                  kichua: 'Mushuk',
                },
                {
                  es: 'Viejo (para personas)',
                  kichua: 'Ruku',
                },
                {
                  es: 'Viejo (para cosas)',
                  kichua: 'Mawka',
                },
                {
                  es: 'Enfermo',
                  kichua: 'Unkushka',
                },
                {
                  es: 'Cansado',
                  kichua: 'Shaykushka',
                },
                {
                  es: 'Lejos ',
                  kichua: 'Karu',
                },
                {
                  es: 'Cerca',
                  kichua: 'Kuchu',
                },
                {
                  es: 'Rápido/pronto',
                  kichua: 'Ucha/ukta',
                },
                {
                  es: 'Lento, despacio',
                  kichua: 'Allimanta',
                },
                {
                  es: 'Inútil',
                  kichua: 'Yanka ',
                },
                {
                  es: 'Sucio',
                  kichua: 'Mapa',
                },

              ]
            },
            {
              numero: 2,
              nombre: 'ANTÓNIMOS',
              diccionario: [
                {
                  es: 'Dentro/Fuera  ',
                  kichua: 'Uku/kancha',
                },
                {
                  es: 'Debajo/Encima',
                  kichua: 'Uku/Jawa',
                },
                {
                  es: 'Pesado/Liviano',
                  kichua: 'LLashak/Pankalla',
                },
                {
                  es: 'Detrás/Delante  ',
                  kichua: 'Washa/ Ñawpa',
                },
                {
                  es: 'Cerca/Lejos',
                  kichua: 'Kuchu/ Karu',
                },
                {
                  es: 'Tonto/Inteligente           ',
                  kichua: 'Muspa/Yuyaysapa',
                },
                {
                  es: 'Pobre/Rico   ',
                  kichua: 'Wakcha/Charik',
                },
                {
                  es: 'Vacío/Lleno',
                  kichua: 'Chushak/Junta',
                },
                {
                  es: 'Despacio/Rápido',
                  kichua: 'Allimanta/Utka(ucha)',
                },
                {
                  es: 'Bonito/Feo  ',
                  kichua: 'Sumak/Mana Sumak',
                },
                {
                  es: 'Bueno/Malo',
                  kichua: 'Alli/Mana Alli-Millay',
                },
                {
                  es: 'Sucio/Limpio',
                  kichua: 'Mapa/Limpio',
                },
                {
                  es: 'Fuerte/Débil  ',
                  kichua: 'Sinchi/Irki',
                },
                {
                  es: 'Difícil/Fáci',
                  kichua: 'Sinchi/Jawalla',
                },
                {
                  es: 'Nuevo/Viejo',
                  kichua: 'Mushuk/Mawka',
                },
                {
                  es: 'Pesado/Liviano',
                  kichua: 'Yashak/Pankalla',
                },
                {
                  es: 'Joven/Anciano(mujer)',
                  kichua: 'Kuytsa/Paya',
                },
                {
                  es: 'Joven/Anciano',
                  kichua: 'Wampra/Ruku',
                },
                {
                  es: 'Gordo/Flaco',
                  kichua: 'Wira/Tsala',
                },
                {
                  es: 'Grande/Pequeño',
                  kichua: 'Jatun/Uchilla',
                },

              ]
            },
            {
              numero: 3,
              nombre: 'VERBOS',
              diccionario: [
                {
                  es: 'Dormir',
                  kichua: 'Puñuna',
                },
                {
                  es: 'Caminar',
                  kichua: 'Purina',
                },
                {
                  es: 'Comer',
                  kichua: 'Mikuna',
                },
                {
                  es: 'Oír   ',
                  kichua: 'Uyana',
                },
                {
                  es: 'Bailar  ',
                  kichua: 'Tushuna',
                },
                {
                  es: 'Ver',
                  kichua: 'Rikuna',
                },
                {
                  es: 'Bañarse',
                  kichua: 'Armana',
                },
                {
                  es: 'Mojarse  ',
                  kichua: 'Shutujyana',
                },
                {
                  es: 'Vivir',
                  kichua: 'kawsana',
                },
                {
                  es: 'Entrar',
                  kichua: 'Yaykuna',
                },
                {
                  es: 'Poner',
                  kichua: 'Churay',
                },
                {
                  es: 'Hacer',
                  kichua: 'Rurana',
                },

              ]
            },
            {
              numero: 4,
              nombre: 'EXPRESIONES ÚTILES ',
              diccionario: [
                {
                  es: 'Katuway',
                  kichua: 'Véndame',
                },
                {
                  es: '¿Imamatak munanki?',
                  kichua: '¿Qué quiere?',
                },
                {
                  es: 'Churashapami',
                  kichua: 'Seguro le pondré ',
                },
                {
                  es: 'Pagui',
                  kichua: 'Gracias',
                },
                {
                  es: 'Pichilla',
                  kichua: 'Muy poquito está',
                },
                {
                  es: '¿Kanka?',
                  kichua: '¿y tú?',
                },
              ]
            },
          ],
          evaluacion: [
            {
              numero: 1,
              texto: 'Shina Shinalla significa Mas o menos',
              tipo: 'Cuestionario',

              respuestaCorrecta: 'true',
              salto: 0,
              nota: 10
            },
            {
              numero: 2,
              texto: 'Ñukata payta rikunkapakmi chayman rini',
              tipo: 'Completación',

              respuestaCorrecta: 'rikunkapakmi',
              salto: 0,
              nota: 10
            }
          ]
        }
      ).save(),

      new Leccion(
        {
          numero: 9,
          nombre: 'Partículas, Vocabulario',
          recursos: [
            {
              name: '1. PARTÍCULAS',
              url: '1. PARTÍCULAS.pdf'
            },
            {
              name: '2. PRONOMBRES INTERROGATIVOS',
              url: '2. PRONOMBRES INTERROGATIVOS.pdf'
            },
            {
              name: '3. PREGUNTAS',
              url: '3. PREGUNTAS.pdf'
            },
            {
              name: '4. VOCABULARIO',
              url: '4. VOCABULARIO.pdf'
            }
          ],
          actividad: `
              • Partículas 
              • Pronombres Interrogativos
              • Preguntas 
              • Vocabulario
          
              a) Actividad de la lección 9
          
          Objetivos:
          En esta unidad aprenderá como se dicen las partículas en Kichwa y también a saber en qué momento utilizar cada una de ellas.
          Aumentará su conocimiento del idioma al aprender nuevas palabras con vocabulario.
          
          Actividad:
              a. Revisar el contenido
              b. Apoyarse en los recursos para complementar el aprendizaje.
              c. Al finalizar, deberá responder el cuestionario para practicar lo aprendido.
          `,
          niveles: [
            {
              numero: 1,
              nombre: 'PARTÍCULAS',
              diccionario: [
                {
                  kichua: '-pa Monicapa misi',
                  es: 'Da la idea de posesión El gato de Mónica ',
                },
                {
                  kichua: '-pash o -pish Nuka mama taytapash',
                  es: 'Significan “y” o “también” Mi madre y padre',
                },
                {
                  kichua: '-pi Ñuka wasipi',
                  es: 'Significa “en” En mi casa',
                },
                {
                  kichua: '-mi Juanka rikunmi',
                  es: 'Da énfasis a la palabra a la cual se añade. Juan si ve.',
                },
                {
                  kichua: '-ka Kanka Kawsankimi',
                  es: 'Identifica quien esta ejecutando la acción. Ej: Tú vienes.',
                },
                {
                  kichua: '-ta Payka ñukata rikunmi',
                  es: 'Identifica quien o que recibe la acción. Ej: ¿A quien ve el? Él me ve a mí. ',
                },
                {
                  kichua: '-chu Kanka allkuta charinkichu?',
                  es: 'Indica que la pregunta espera una respuesta de si o no. Ej. Tú perro tienes?',
                },
                {
                  kichua: '-chu (acompañada de mana) Paytaka mana juyanchu.',
                  es: 'Significa negación Él no la ama.',
                },
                {
                  kichua: '-tak ¿Pitak kan?',
                  es: 'Se añade a las palabras interrogativas como un signo de interrogación verbal Ej. ¿Quién es?',
                },
                {
                  kichua: '-ta Lunestami shamunka',
                  es: 'Significa “a” cuando hablamos de tiempo. Él viene el lunes.',
                },

              ]
            },
            {
              numero: 2,
              nombre: 'PRONOMBRES INTERROGATIVOS',
              diccionario: [
                {
                  es: 'Que',
                  kichua: 'Ima',
                },
                {
                  es: 'Cuál',
                  kichua: 'Maykan',
                },
                {
                  es: 'Quién',
                  kichua: 'Pi',
                },
                {
                  es: 'De quién',
                  kichua: 'Pipactak',
                },
                {
                  es: 'Por qué',
                  kichua: 'Imamanta',
                },
                {
                  es: 'Cómo',
                  kichua: 'Imashina',
                },
                {
                  es: 'En dónde',
                  kichua: 'Maypi',
                },
                {
                  es: 'De dónde ',
                  kichua: 'Maymanta',
                },
                {
                  es: 'A donde',
                  kichua: 'Mayman',
                },
                {
                  es: 'Cúales',
                  kichua: 'Maykankuna',
                },
                {
                  es: 'Cuándo',
                  kichua: 'Ima hora',
                },
                {
                  es: 'Sí',
                  kichua: 'Ari',
                },
                {
                  es: 'No',
                  kichua: 'Mana',
                },

              ]
            },
            {
              numero: 3,
              nombre: 'PREGUNTAS',
              diccionario: [
                {
                  es: '¿Quién es?',
                  kichua: 'Pitak kan?',
                },
                {
                  es: '¿En dónde es?',
                  kichua: 'Maypitak kan?',
                },
                {
                  es: '¿De dónde eres?',
                  kichua: 'Maymantatak kanki?',
                },
                {
                  es: '¿Quién eres?',
                  kichua: 'Pitak kanki?',
                },
                {
                  es: '¿Qué es?',
                  kichua: 'Imatak kan?',
                },
                {
                  es: '¿A dónde vas?',
                  kichua: 'Maymantak rinki?',
                },
                {
                  es: '¿Cuál es?',
                  kichua: 'Maikantak kan?',
                },
                {
                  es: '¿Por qué estamos aquí?    ',
                  kichua: 'Imamantak kaypi kankichik?',
                },
                {
                  es: '¿En dónde vives?',
                  kichua: 'Maypitak kawsanki?',
                },
                {
                  es: '¿Por qué no vino?',
                  kichua: 'Imamantatak mana shamurka? ',
                },
              ]
            },
            {
              numero: 4,
              nombre: 'VOCABULARIO',
              diccionario: [
                {
                  es: 'Bastantes',
                  kichua: 'Tawka',
                },
                {
                  es: 'Pregunta',
                  kichua: 'Tapuy',
                },
                {
                  es: 'Por eso',
                  kichua: 'Chaymanta',
                },
                {
                  es: 'Después ',
                  kichua: 'Kipa',
                },
                {
                  es: 'Antes',
                  kichua: 'Ñawpa',
                },
                {
                  es: 'Día ',
                  kichua: 'puncha',
                },
                {
                  es: 'Mes',
                  kichua: 'Killa',
                },
                {
                  es: 'Año',
                  kichua: 'Wata',
                },
                {
                  es: 'Hoy, hoy en día ',
                  kichua: 'Kunan puncha',
                },
                {
                  es: 'Mañana ',
                  kichua: 'Kaya ',
                },
                {
                  es: 'Persona',
                  kichua: 'Runa',
                },
                {
                  es: 'Personas',
                  kichua: 'Runakuna',
                },
                {
                  es: 'Varios',
                  kichua: 'Tawka',
                },
                {
                  es: 'Suficiente, bastante',
                  kichua: 'Ninanta',
                },
                {
                  es: 'Aquí',
                  kichua: 'Kaypi',
                },
                {
                  es: 'Ahí ',
                  kichua: 'Chaypi',
                },
                {
                  es: 'Ahora',
                  kichua: 'Kunan',
                },

              ]
            },
          ],
          evaluacion: [
            {
              numero: 1,
              texto: 'Shina Shinalla significa Mas o menos',
              tipo: 'Cuestionario',

              respuestaCorrecta: 'true',
              salto: 0,
              nota: 10
            },
            {
              numero: 2,
              texto: 'Ñukata payta rikunkapakmi chayman rini',
              tipo: 'Completación',

              respuestaCorrecta: 'rikunkapakmi',
              salto: 0,
              nota: 10
            }
          ]
        }
      ).save(),

      new Leccion(
        {
          numero: 10,
          nombre: 'Verbos, Partículas etc',
          recursos: [
            {
              name: '1. PASADO TERMINACIONES PARA LOS VERBOS SEGÚN LOS PRONOMBRES',
              url: '1. PASADO TERMINACIONES PARA LOS VERBOS SEGÚN LOS PRONOMBRES.pdf'
            },
            {
              name: '2. PASADO CONJUGACIÓN DEL VERBO DECIR',
              url: '2. PASADO CONJUGACIÓN DEL VERBO DECIR.pdf'
            },
            {
              name: '3. VERBOS',
              url: '3. VERBOS.pdf'
            },
            {
              name: '4.PARTÍCULAS',
              url: '4.PARTÍCULAS.pdf'
            }
          ],
          actividad: `
            • Terminación para los verbos según los pronombres en Tiempo Pasado
            • Conjugación del verbo decir en Tiempo Pasado
            • Verbos
            • Partículas

            a) Actividad de la lección 10

        Objetivos:
        En esta unidad aprenderá a conjugar los verbos en tiempo pasado, para ello lo hare usando el Verbo Decir (Nina)
        Además, aprenderá a usar las Partículas y también seguirá aumentando su conocimiento con mas Verbos.

        Actividad:
            a. Revisar el contenido
            b. Apoyarse en los recursos para complementar el aprendizaje.
            c. Al finalizar, deberá responder el cuestionario para practicar lo aprendido.
          `,
          niveles: [
            {
              numero: 1,
              nombre: 'TERMINACIONES PARA LOS VERBOS SEGÚN LOS PRONOMBRES EN TIEMPO PASADO',
              diccionario: [
                {
                  kichua: 'Ñuka',
                  es: '-rkani',
                },
                {
                  kichua: 'Kan,Kikin',
                  es: '-rkanki',
                },
                {
                  kichua: 'Pay',
                  es: '-rka',
                },
                {
                  kichua: 'Ñukanchik',
                  es: '-rkanchik',
                },
                {
                  kichua: 'Kankuna, Kikinkuna',
                  es: '-rkankichik',
                },
                {
                  kichua: 'Paykuna',
                  es: '-rkakuna/-rka',
                },
              ]
            },
            {
              numero: 2,
              nombre: 'PASADO: CONJUGACIÓN DEL VERBO DECIR',
              diccionario: [
                {
                  es: 'Yo dije',
                  kichua: 'Ñuka nirkani',
                },
                {
                  es: 'Tú dijiste',
                  kichua: 'Kan/kikin nirkanki',
                },
                {
                  es: 'Él dijo',
                  kichua: 'Pay nirka',
                },
                {
                  es: 'Nosotros dijimos',
                  kichua: 'Ñukanchik nirkanchik',
                },
                {
                  es: 'Ustedes dijeron',
                  kichua: 'Kankuna/ kikinkuna nirkankichik',
                },
                {
                  es: 'Ellos dijeron',
                  kichua: 'Paykuna nirka/ nirkakuna',
                },

              ]
            },
            {
              numero: 3,
              nombre: 'VERBOS',
              diccionario: [
                {
                  es: 'Querer',
                  kichua: 'Kuyana/Munana',
                },
                {
                  es: 'acción de estar listo, ya',
                  kichua: 'Ñachu',
                },
                {
                  es: 'Sentarse',
                  kichua: 'Chachina',
                },
                {
                  es: 'Reventar',
                  kichua: 'Tukyay',
                },
                {
                  es: 'Dar',
                  kichua: 'kuna',
                },
                {
                  es: 'Tener',
                  kichua: 'Charina',
                },
                {
                  es: 'Venir',
                  kichua: 'Shamuna',
                },
                {
                  es: 'Ir',
                  kichua: 'Rina',
                },
                {
                  es: 'Permitir',
                  kichua: 'Sakina',
                },
                {
                  es: 'Saber',
                  kichua: 'Yachana',
                },
                {
                  es: 'Poder',
                  kichua: 'Pudina',
                },
                {
                  es: 'Crear',
                  kichua: 'Ushana',
                },
                {
                  es: 'Crecer',
                  kichua: 'Wiñana',
                },
                {
                  es: 'Aprender',
                  kichua: 'Yachakuna',
                },
                {
                  es: 'Existir',
                  kichua: 'Tiyana',
                },
                {
                  es: 'Creer',
                  kichua: 'Krina',
                },
                {
                  es: 'Poner ',
                  kichua: 'Churana',
                },
                {
                  es: 'Lavar la cara o manos',
                  kichua: 'Mayllana ',
                },
                {
                  es: 'Lavar ropa',
                  kichua: 'Takshana',
                },

              ]
            },
            {
              numero: 4,
              nombre: 'PARTÍCULAS',
              diccionario: [
                {
                  kichua: '-man ¿Maymantak rinki? Wasiman',
                  es: 'Indica la dirección hacia la cual la persona u objeto se dirige. Ej. ¿A dónde vas? A mi casa.',
                },
                {
                  kichua: '-pa ¿Ima shutitak kapanki?',
                  es: 'Se coloca entre la raíz del verbo y su terminación para mostrar respeto. Ej. ¿Cómo te llamas? ',
                },
                {
                  kichua: '-chi Mikuchina',
                  es: 'En medio del verbo indica que una persona esta haciendo que otra complete la acción Ej. Hacer que otro coma',
                },
                {
                  kichua: '-kapak Ñukata payta rikunkapakmi chayman rini.',
                  es: 'Significa “para” indicando propósito. Ej. Yo voy allá para verlo.',
                },
                {
                  kichua: '-yuk Warmiyuk',
                  es: 'Denota “dueño” o “posedor” Hombre casado',
                },
                {
                  kichua: '-yari Shamuyari',
                  es: 'Se utiliza para re enfatizar la orden. Es mas fuerte que Shamuy',
                },
                {
                  kichua: '-cha/chari Paycha rimarka',
                  es: 'Significa “probablemente” Tal vez él habló.',
                },
                {
                  kichua: '-Kama Quitokamami rinchik.',
                  es: 'Significa “hasta” Vamos hasta Quito',
                }
              ]
            },
          ],
          evaluacion: [
            {
              numero: 1,
              texto: 'Shina Shinalla significa Mas o menos',
              tipo: 'Cuestionario',

              respuestaCorrecta: 'true',
              salto: 0,
              nota: 10
            },
            {
              numero: 2,
              texto: 'Ñukata payta rikunkapakmi chayman rini',
              tipo: 'Completación',

              respuestaCorrecta: 'rikunkapakmi',
              salto: 0,
              nota: 10
            }
          ]
        }
      ).save(),

      new Leccion(
        {
          numero: 11,
          nombre: 'Oraciones Imperativas',
          recursos: [
            {
              name: '1. FUTURO TERMINACIONES PARA LOS VERBOS SEGÚN LOS PRONOMBRES',
              url: '1. FUTURO TERMINACIONES PARA LOS VERBOS SEGÚN LOS PRONOMBRES.pdf'
            },
            {
              name: '2. VOCABULARIOS',
              url: '2. VOCABULARIOS.pdf'
            },
            {
              name: '3. VERBOS',
              url: '3. VERBOS.pdf'
            },
            {
              name: '4. ORACIONES IMPERATIVAS',
              url: '4. ORACIONES IMPERATIVAS.pdf'
            },
            {
              name: '5. PARTÍCULAS',
              url: '5. PARTÍCULAS.pdf'
            }
          ],
          actividad: `
                  LECCIÓN 11

            • Futuro: terminación de los verbos según los pronombres
            • Vocabulario
            • Verbos
            • Oraciones Imperativas
            • Partículas 

            a) Actividad de la lección 11

        Objetivos:
        En esta unidad aprenderá a conjugar los verbos en tiempo FUTURO. 
        Además, aprenderá más partículas que se usan en el Kichwa y también conocerá el uso de las oraciones imperativas.

        Actividad:
            a. Revisar el contenido
            b. Apoyarse en los recursos para complementar el aprendizaje.
            c. Al finalizar, deberá responder el cuestionario para practicar lo aprendido.
          `,
          niveles: [
            {
              numero: 1,
              nombre: 'FUTURO TERMINACIONES PARA LOS VERBOS SEGÚN LOS PRONOMBRES',
              diccionario: [
                {
                  es: 'Ñuka',
                  kichua: '-sha',
                },
                {
                  es: 'Kan,Kikin',
                  kichua: '-nki',
                },
                {
                  es: 'Pay',
                  kichua: '-nka',
                },
                {
                  es: 'Ñukanchik',
                  kichua: '-shun',
                },
                {
                  es: 'Kankuna, Kikinkuna',
                  kichua: '-nkichik',
                },
                {
                  es: 'Paykuna',
                  kichua: '-nkakuna/-nka',
                },
              ]
            },
            {
              numero: 2,
              nombre: 'VOCABULARIO',
              diccionario: [
                {
                  es: 'Cuanto, cuantos',
                  kichua: 'Mashna',
                },
                {
                  es: 'Inicuo',
                  kichua: 'Millay',
                },
                {
                  es: 'Muchacho, joven masculino',
                  kichua: 'Wampra',
                },
                {
                  es: 'Muchacha, joven, femenino',
                  kichua: 'Kuytsa',
                },
                {
                  es: 'Arriba/encima',
                  kichua: 'Jawa',
                },
                {
                  es: 'Abajo/debajo',
                  kichua: 'Ura',
                },
                {
                  es: 'Adentro',
                  kichua: 'Ukupi',
                },
                {
                  es: 'Afuera',
                  kichua: 'Kancha',
                },
                {
                  es: 'Izquierda ',
                  kichua: 'Lluki lado',
                },
                {
                  es: 'Derecha',
                  kichua: 'Alli lado',
                },
                {
                  es: 'más',
                  kichua: 'ashtawan',
                },
                {
                  es: 'Animal ',
                  kichua: 'wiwa',
                },
                {
                  es: 'No todavia',
                  kichua: 'manarak',
                },

              ]
            },
            {
              numero: 3,
              nombre: 'VERBOS',
              diccionario: [
                {
                  es: 'Mostrar',
                  kichua: 'Rikuchina',
                },
                {
                  es: 'Regalar',
                  kichua: 'Karana',
                },
                {
                  es: 'Comprar',
                  kichua: 'Rantina',
                },
                {
                  es: 'Vender',
                  kichua: 'Katuna',
                },
                {
                  es: 'Salir',
                  kichua: 'lluksina',
                },
                {
                  es: 'Faltar, no haber',
                  kichua: 'Illna',
                },
                {
                  es: 'Dar',
                  kichua: 'Kuna',
                },
                {
                  es: 'Caminar',
                  kichua: 'Purina',
                },
                {
                  es: 'Recordar',
                  kichua: 'Yuyarina',
                },
                {
                  es: 'Correr',
                  kichua: 'Kallpana',
                },
                {
                  es: 'Gobernar, mandar',
                  kichua: 'Mandana ',
                },
                {
                  es: 'Cuidar',
                  kichua: 'Cuidana',
                },
                {
                  es: 'Edificar, construir',
                  kichua: 'shayachina',
                },
                {
                  es: 'Nombrar',
                  kichua: 'Shutichina',
                },
              ]
            },
            {
              numero: 4,
              nombre: 'ORACIONES IMPERATIVAS',
              diccionario: [
                {
                  es: 'Para los verbos que termina en -ANA o -UNA:',
                  kichua: '',
                },
                {
                  es: 'Cuando la orden se dirige a una persona ¡Ven!:   ¡Come!: ¡Mire por favor!:',
                  kichua: 'se remueve el -na al final del verbo y se lo reemplaza con -y. ¡Shamuy! ¡Mikuy! ¡Rikupay!',
                },
                {
                  es: 'Si la orden se dirige a dos o mas personas: ¡Vengan! ¡Coman! ¡Vean, por favor',
                  kichua: 'Se remueve el -na final del verbo y se lo reemplaza con -ychink. ¡Shamuychik! ¡Mikuychik! ¡Rikupaychik!',
                },
                {
                  es: 'Para los verbos que terminan en -INA:',
                  kichua: '',
                },
                {
                  es: 'Cuando la orden se dirige a una persona ¡Vé! ¡Sal!',
                  kichua: 'se remueve el -na al final del verbo y ya está. ¡Ri! ¡Llukshi! ',
                },
                {
                  es: 'Si la orden se dirige a dos o mas personas: ¡Ve! ¡Sal!',
                  kichua: 'Simplemente se le añade -chik para pluralizarla. ¡Richik! ¡Llukshichik!',
                },
              ]
            },
            {
              numero: 5,
              nombre: 'PARTÍCULAS',
              diccionario: [
                {
                  kichua: '-Lla',
                  es: 'Tiene varios significados.     1.  Unico, solo, sin ayuda. Paylla shamurka (Él vino solo)     2. Ternura, cariño, o afecto. Mamalla (mamita)     3. Indica la facilidad con la que se hace algo, sin dificultad. ¡Rikuylla! (Mira no más)     4. Se utiliza para formar adverbios. Llakilla (tristemente)     5. Exacatmente o mismo Shinallatakmi (asi mismo, exactamente)     6. La persona misma  Ñukallatak (yo mismo)',
                },
                {
                  kichua: '-Pura Ejemplo: Wawkipura',
                  es: 'Significa “entre” y da la idea de cooperación. Entre hermanos',
                },
                {
                  kichua: '-Mari',
                  es: 'Indica la idea de algo absoluto o definitivo',
                },
                {
                  kichua: '-Sapa Ejemplo: Umasapa',
                  es: 'Se usa principalmente con las partes del cuerpo para indicar que son mas grande de lo normal. Ej. Cabezón',
                },
                {
                  kichua: '-ya Rukuyana',
                  es: 'Significa “llegar a ser” Llegar a ser viejo',
                },
                {
                  kichua: '-naku Kawsanakuna',
                  es: 'Solo se usa con verbos. Denota participación mutua o entre grupo. :Vivir junto con otros. ',
                },
              ]
            }
          ],
          evaluacion: [
            {
              numero: 1,
              texto: 'Shina Shinalla significa Mas o menos',
              tipo: 'Cuestionario',

              respuestaCorrecta: 'true',
              salto: 0,
              nota: 10
            },
            {
              numero: 2,
              texto: 'Ñukata payta rikunkapakmi chayman rini',
              tipo: 'Completación',

              respuestaCorrecta: 'rikunkapakmi',
              salto: 0,
              nota: 10
            }
          ]
        }
      ).save(),

      new Leccion(
        {
          numero: 12,
          nombre: 'Hacer Comparaciones',
          recursos: [
            {
              name: '1. VERBOS',
              url: '1. VERBOS.pdf'
            },
            {
              name: '2. VOCABULARIOS',
              url: '2. VOCABULARIOS.pdf'
            },
            {
              name: '3. HACER COMPARACIONES',
              url: '3. HACER COMPARACIONES.pdf'
            },
            {
              name: '4. PARTÍCULAS',
              url: '4. PARTÍCULAS.pdf'
            }
          ],
          actividad: ` 
           LECCIÓN 12

              • Verbos
              • Vocabulario
              • Hacer comparaciones
              • Partículas 

              a) Actividad de la lección 12

          Objetivos:
          En esta unidad aprenderá más vocabulario para poder usar en el día a día. 
          Aprenderá como hacer comparaciones usando las partículas.

          Actividad:
              a. Revisar el contenido
              b. Apoyarse en los recursos para complementar el aprendizaje.
              c. Al finalizar, deberá responder el cuestionario para practicar lo aprendido.
          `,
          niveles: [
            {
              numero: 1,
              nombre: 'VERBOS',
              diccionario: [
                {
                  es: 'Esperar',
                  kichua: 'Shuyana',
                },
                {
                  es: 'Pararse',
                  kichua: 'Shayarina',
                },
                {
                  es: 'Descansar',
                  kichua: 'Samana',
                },
                {
                  es: 'Sentarse ',
                  kichua: 'Tiyarina',
                },
                {
                  es: 'Quebrar, romper',
                  kichua: 'Pakina',
                },
                {
                  es: 'Reunir, recoger',
                  kichua: 'Tantachina',
                },
                {
                  es: 'Aumentar, multiplicar',
                  kichua: 'Mirachina',
                },
                {
                  es: 'Reunirse',
                  kichua: 'Tantanakuna',
                },
                {
                  es: 'Mentir',
                  kichua: 'Llullana',
                },
                {
                  es: 'Dividir',
                  kichua: 'Rakina',
                },
                {
                  es: 'Pelear',
                  kichua: 'Makanakuna',
                },
                {
                  es: 'Soñar',
                  kichua: 'Muskuna',
                },
                {
                  es: 'Jugar',
                  kichua: 'Pukllana',
                },
                {
                  es: 'Cortar',
                  kichua: 'Kuchuna',
                },
                {
                  es: 'Acabar, destruir',
                  kichua: 'Tukuchina',
                },
                {
                  es: 'Acabarse, terminarse',
                  kichua: 'Tukurina',
                },
                {
                  es: 'Cocinar',
                  kichua: 'Yanuna',
                },

              ]
            },
            {
              numero: 2,
              nombre: 'VOCABULARIO',
              diccionario: [
                {
                  es: 'Entonces',
                  kichua: 'Shinaka',
                },
                {
                  es: 'Pecado',
                  kichua: 'jucha',
                },
                {
                  es: 'Otra vez',
                  kichua: 'kutin',
                },
                {
                  es: 'Largo tiempo',
                  kichua: 'Unay ',
                },
                {
                  es: 'Reunión',
                  kichua: 'Tantanakuy',
                },
                {
                  es: 'Otro',
                  kichua: 'Shuktak',
                },
                {
                  es: 'Algunos',
                  kichua: 'Wakin',
                },
                {
                  es: 'Cada uno',
                  kichua: 'Shuk shuk',
                },
                {
                  es: 'Este otro',
                  kichua: 'Kayshuk',
                },
                {
                  es: 'Ese otro',
                  kichua: 'Cahyshuk',
                },
                {
                  es: 'A veces, de vez en cuando ',
                  kichua: 'wakinpi',
                },
              ]
            },
            {
              numero: 3,
              nombre: 'HACER COMPARACIONES',
              diccionario: [
                {
                  es: 'Hay dos maneras de hacer comparaciones:',
                  kichua: '',
                },
                {
                  es: `1. Añadiendole la particula. -manta a la persona o cosa que se esta comparando procedida de la palabra ashtawan (más)
                       2. Añadiendo la particula -ta a la persona o cosa que se esta comparando procedida por la palabra yalli.`,
                  kichua: `1. Miguelka Juanmanta ashtawan jatunmi kan. (Miguel es mas grande que Juan)     
                           2. Miguelka Juanta yalli jatunmi kan. (Miguel es mas grande que Juan)`,
                },
                {
                  es: 'Pero si las dos personas o cosas que se están comparando son iguales se le añade la partícula -shina',
                  kichua: 'Miguelka Juan shinami uchilla kan. (Miguel es igual de grande que Juan)',
                },
              ]
            },
            {
              numero: 4,
              nombre: 'PARTÍCULAS',
              diccionario: [
                {
                  es: '-k Yachachik',
                  kichua: 'Personifica el verbo, significa “el que” o “la que” : el que o la que enseña',
                },
                {
                  es: '-pacha Excelente',
                  kichua: 'Denota lo completo o plenitud. Allipacha ',
                },
                {
                  es: '-Laya  ¿Imlayatak?',
                  kichua: 'Se puede traducir “como”, “de cierto tipo o clase” ¿ De que clase, de que tipo?',
                },
                {
                  es: '-chari (-cha) -shi ¿Pay shamunkacha?',
                  kichua: 'Se utiliza para expresar duda. ¿Él quizás?',
                },
                {
                  es: '-naya Yakunayami',
                  kichua: 'Significa “dar ganas de” Tengo sed',
                },
                {
                  es: '-ntin Discipulokunantinmi rirka',
                  kichua: 'Transmite la idea de junto. Fue junto con sus discípulos.',
                },
                {
                  es: '-rayku Ñukanchikka payraykumi chayta rurarkanchik.',
                  kichua: 'Indica motivo, objetivo, o causa. Nosotros hicimos eso por ella.',
                },
                {
                  es: '-pash Imapash Mana Imapash Pipash Maykanpash Imashinapash',
                  kichua: 'Se usa para formar pronombres indefinidos. Lo que sea Nada en absoluto Quien sea Cual sea Como sea',
                },

              ]
            },
          ],
          evaluacion: [
            {
              numero: 1,
              texto: 'Shina Shinalla significa Mas o menos',
              tipo: 'Cuestionario',

              respuestaCorrecta: 'true',
              salto: 0,
              nota: 10
            },
            {
              numero: 2,
              texto: 'Ñukata payta rikunkapakmi chayman rini',
              tipo: 'Completación',

              respuestaCorrecta: 'rikunkapakmi',
              salto: 0,
              nota: 10
            }
          ]
        }
      ).save(),

      new Leccion(
        {
          numero: 13,
          nombre: 'Expresiones Útiles, etc',
          recursos: [
            {
              name: '1. EXPRESIONES ÚTILES',
              url: '1. EXPRESIONES ÚTILES.pdf'
            },
            {
              name: '2. VERBOS',
              url: '2. VERBOS.pdf'
            },
            {
              name: '3. VOCABULARIO',
              url: '3. VOCABULARIO.pdf'
            },
            {
              name: '4. PARTÍCULAS',
              url: '4. PARTÍCULAS.pdf'
            }
          ],
          actividad: `
                    LECCIÓN 13

              • Expresiones útiles
              • Verbos
              • Vocabulario
              • Partículas 

              a) Actividad de la lección 13

          Objetivos:
          En esta unidad usted conocerá como se dicen en Kichwa algunas expresiones útiles que le ayudaran a comunicarse. 
          Podrá aumentar su conocimiento al aprender más Vocabulario y también mas verbos.

          Actividad:
              a. Revisar el contenido
              b. Apoyarse en los recursos para complementar el aprendizaje.
              c. Al finalizar, deberá responder el cuestionario para practicar lo aprendido.

          `,
          niveles: [
            {
              numero: 1,
              nombre: 'EXPRESIONES ÚTILES',
              diccionario: [
                {
                  es: '¡Maytashi!',
                  kichua: '¿Cómo va a ser posible? !Es increíble! !De ninguna manera! !Que va!',
                },
                {
                  es: 'Minkachiway',
                  kichua: 'Déjenme pasar',
                },
                {
                  es: 'Perdonaway',
                  kichua: 'Perdonenme ',
                },
                {
                  es: '¡Llukshi!',
                  kichua: '¡Sal!',
                },
                {
                  es: '¡Rikuy!',
                  kichua: '¡Mira!',
                },
                {
                  es: '¡Upalla!',
                  kichua: '¡Silencio!',
                },
                {
                  es: '¡Saki!',
                  kichua: '¡Deja!',
                },
                {
                  es: '¡Apuray!',
                  kichua: '¡Apúrate!',
                },
                {
                  es: '¡Shuyay!',
                  kichua: '¡Espera!',
                },
                {
                  es: '¡Kasilla kay!',
                  kichua: '¡Quédate tranquilo! (niños)',
                },
                {
                  es: '¡Achachay!',
                  kichua: '¡Qué frio!',
                },
                {
                  es: '¡Arraray!',
                  kichua: '¡Qué calor!',
                },
                {
                  es: '¡Ananay!',
                  kichua: '¡Qué bonito!',
                },
                {
                  es: '¡Kaygupi tiyaripay!',
                  kichua: '¡Siéntese aquí!',
                },
                {
                  es: '¡Jaku! ¡Jakuchik!',
                  kichua: '¡Vamos, vámonos!',
                },

              ]
            },
            {
              numero: 2,
              nombre: 'VERBOS',
              diccionario: [
                {
                  es: 'Bañar',
                  kichua: 'Armana',
                },
                {
                  es: 'Levantarse',
                  kichua: 'Jatarina',
                },
                {
                  es: 'Casarse',
                  kichua: 'Kasarana',
                },
                {
                  es: 'Acercarse',
                  kichua: 'Kimirina',
                },
                {
                  es: 'Verse, aparecer',
                  kichua: 'Rikurina',
                },
                {
                  es: 'Obedecer ',
                  kichua: 'Kasuna',
                },
                {
                  es: 'Llorar',
                  kichua: 'Wakana',
                },
                {
                  es: 'Coger, agarrar',
                  kichua: 'Japina',
                },
                {
                  es: 'Llover',
                  kichua: 'Tamiyana',
                },
                {
                  es: 'Olvidar ',
                  kichua: 'Kunkana ',
                },
                {
                  es: 'Hacer bien, arreglar, reparar ',
                  kichua: 'Allichina',
                },
                {
                  es: 'Comparar',
                  kichua: 'Chimbapurana',
                },
                {
                  es: 'Ocultar, esconder',
                  kichua: 'Pakana',
                },
                {
                  es: 'Pasar',
                  kichua: 'Yallina',
                },
                {
                  es: 'Sentir',
                  kichua: 'Sintirina',
                },
                {
                  es: 'Gritar',
                  kichua: 'Kaparina',
                },
                {
                  es: 'Bailar',
                  kichua: 'Tushuna',
                },
                {
                  es: 'Enviar',
                  kichua: 'Kachana',
                },
                {
                  es: 'Guiar, dirigir',
                  kichua: 'Pushana',
                },
                {
                  es: 'Tirar, botar',
                  kichua: 'Shitana',
                },
                {
                  es: 'Quedarse',
                  kichua: 'Sakirina',
                },
                {
                  es: 'Hacer doler, causar dolor',
                  kichua: 'Nanachina',
                },
                {
                  es: 'Llegar a ser, suceder',
                  kichua: 'Tukuna',
                },
                {
                  es: 'Orar',
                  kichua: 'Mañana',
                },

              ]
            },
            {
              numero: 3,
              nombre: 'COSAS DEL AULA',
              diccionario: [
                {
                  es: 'En vez de, en lugar de ',
                  kichua: 'Ranti ',
                },
                {
                  es: 'Lágrima ',
                  kichua: 'Wiki ',
                },
                {
                  es: 'Acá ',
                  kichua: 'kayman',
                },
                {
                  es: 'Allá',
                  kichua: 'Chayman',
                },
                {
                  es: 'Alrededor',
                  kichua: 'Muyuntin ',
                },
                {
                  es: 'En frente ',
                  kichua: 'chimba',
                },
                {
                  es: '',
                  kichua: 'Wakin',
                },
                {
                  es: 'Cada uno',
                  kichua: 'Shuk shuk',
                },
                {
                  es: 'Este otro',
                  kichua: 'Kayshuk',
                },
                {
                  es: 'Ese otro',
                  kichua: 'Cahyshuk',
                },
                {
                  es: 'A veces, de vez en cuando ',
                  kichua: 'wakinpi',
                },
              ]
            },
            {
              numero: 4,
              nombre: 'PARTÍCULAS',
              diccionario: [

                {
                  es: '-riya Payka ñukanchikta cuidariyakunmi',
                  kichua: 'Denota hábito o constancia  Él constantemente nos esta cuidando.',
                },
                {
                  es: '-wa Payka kuyawanmi ',
                  kichua: 'Dirije el objeto a mi o a nosotros Él me ama.',
                },
                {
                  es: '-n/ntin Wasin wasin Punchan punchan',
                  kichua: 'Se añade al final de los sustantivos.Da la ide de “uno tras otro” o “ de -en”, también se puede utilizar para expresar totalidad. De casa en casa Cada dia',
                },
                {
                  es: '-nik Chaynikman Kaynikman Janaknikman chynikpi',
                  kichua: 'Indica por el lado de o hacia donde se dirige la acción. Por allá Por acá Hacia arriba Por ahí ',
                },
                {
                  es: '-rak    1. Ñukarakmi rimasha.    2. Payka kaypi kanrak.',
                  kichua: 'Se la añade al final de los sustantivos y verbos. Tiene 2 significados.     1. Indica prioridad, lo que “viene primero” Yo hablare primero.     2. “Aún” o “todavía”  Él todavía está aquí.',
                },
                {
                  es: '-manta Imamanta',
                  kichua: 'Se usa para expresar la causa o el porque de algo. Porque ',
                },
                {
                  es: '-mu Apamuna Chayamuna',
                  kichua: 'Transmite la idea de venir o volver aquí.  Traer aquí  Llegar aquí',
                },
                {
                  es: '-ta Pay shamukta rikunimi. ',
                  kichua: 'Tambien sirve para conectar frases. Yo lo veo venir: yo veo que él viene.',
                },
                {
                  es: '-karin Kunankarin payka shamunka.',
                  kichua: 'Significa “por lo menos, mas aún, especialmente, en cambio”. Esta vez o por fin ahora él vendrá.',
                },
              ]
            }
          ],
          evaluacion: [
            {
              numero: 1,
              texto: 'Shina Shinalla significa Mas o menos',
              tipo: 'Cuestionario',

              respuestaCorrecta: 'true',
              salto: 0,
              nota: 10
            },
            {
              numero: 2,
              texto: 'Ñukata payta rikunkapakmi chayman rini',
              tipo: 'Completación',

              respuestaCorrecta: 'rikunkapakmi',
              salto: 0,
              nota: 10
            }
          ]
        }
      ).save(),

    ]);

    console.log('LECCIONES CREADAS');

  } catch (error) {
    console.error(error);
  }
};