import User from '../models/User'
import Role from '../models/Role'

export const isAdmin = async (req, res, next) => {
    const user = await User.findById(req.body.userId);
    if (!user) {
        return res.status(403).json({ message: "Not user" })
    }
    const rolesFound = await Role.find({ _id: { $in: user.roles } });
    const roles = rolesFound.map(role => role.name);

    if (!roles.includes('admin')) {
        return res.status(403).json({ message: "Requiere ser admin" })
    }

    next();

};