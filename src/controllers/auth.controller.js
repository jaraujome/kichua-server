import User from '../models/User'
import jwt from 'jsonwebtoken'
import config from '../config'
import Role from '../models/Role'

// Registro
export const signUp = async (req, res) => {

    const userFound = await User.findOne({ email: req.body.email })
    
    if (userFound) return res.status(400).json({ message: "Email ya esta registrado" })

    const { username, name, lastName, email, password, roles } = req.body
    const newUser = new User({ username, name, lastName, email, password, roles })
    newUser.password = await newUser.encryptPassword(password);

    if (roles) {
        const foundRoles = await Role.find({ name: { $in: roles } })
        newUser.roles = foundRoles.map(role => role._id)
    } else {
        const role = await Role.findOne({ name: "usuario" });
        newUser.roles = [role._id];
    }

    const savedUser = await newUser.save();
    const token = jwt.sign({ id: savedUser._id }, config.SECRET, {
        expiresIn: 86400
    })

    res.json({ token });
}

// Login
export const signin = async (req, res) => {

    const userFound = await (await User.findOne({ email: req.body.email }))

    if (!userFound) return res.status(400).json({ message: "Usuario no existe" })
    const matchPassword = await userFound.comparePassword(req.body.password, userFound.password)
    if (!matchPassword) return res.status(401).json({ token: null, message: 'Contraseña incorrecta' })

    const token = jwt.sign({ id: userFound._id }, config.SECRET, {
        expiresIn: 86400
    })

    const rolesFound = await Role.find({ _id: { $in: userFound.roles } });
    const roles = rolesFound.map(role => role.name);
    res.status(200).json({ token, _id: userFound._id, email: userFound.email, roles });

}

// obtener usuarios no Admin
export const getUsers = async (req, res) => {

    const role = await Role.findOne({ name: "usuario" });
    const users = await User.aggregate([{ $match: { roles: role._id } }]);
    res.json(users)
}

// obtener todos los usuarios
export const getAllUsers = async (req, res) => {
    const users = await User.find();
    res.json(users)
}
