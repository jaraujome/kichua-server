import LeccionInstance from '../models/LeccionInstance';
import Leccion from '../models/Leccion';
import User from '../models/User';

const fs = require('fs');
let ejs = require("ejs");
let pdf = require("html-pdf");
let path = require("path");
const NOTA_MINIMA = 14;

// obtener todas las leccionInstances (lecciones de los usuarios)
export const getLeccionInstances = async (req, res) => {
    const lecciones = await LeccionInstance.find()
    res.json(lecciones)
}

// obtener todas las lecciones de un usuario
export const getLeccionInstancesByUser = async (req, res) => {

    let leccionesUser = await LeccionInstance.find({ user: req.params.userId }).sort({ numero: 1 })

    // si no tiene lecciones le crea la primera leccion al usuario
    if (!leccionesUser.length) {

        const leccionOne = await Leccion.findOne({ numero: 1 });

        const leccionInstace = new LeccionInstance({
            nombre: leccionOne.nombre,
            numero: leccionOne.numero,
            recursos: leccionOne.recursos,
            actividad: leccionOne.actividad,
            niveles: leccionOne.niveles,
            evaluacion: leccionOne.evaluacion,
            user: req.params.userId
        });

        const user = await User.findById(req.params.userId);
        user.currentLeccion = leccionOne.numero;
        await user.save();

        await leccionInstace.save();

        leccionesUser = await LeccionInstance.find({ user: req.params.userId })
    }

    res.status(200).json(leccionesUser)

}

// obtener y/o generar certificado
export const getCertificado = async (req, res) => {

    const user = await User.findById(req.params.userId);

    // si ya creo el certificado lo envia
    if (user.notaFinal && fs.existsSync(path.resolve(__dirname, '../uploads/certificados/', user._id + '.pdf'))) {
        res.sendFile(path.resolve(__dirname, '../uploads/certificados/', user._id + '.pdf'));
    }

    const leccionesUser = await LeccionInstance.find({ user: req.params.userId })

    if (leccionesUser.length) {

        let sum = leccionesUser.reduce(function (acc, leccion) {
            return acc + (leccion.nota ? leccion.nota : 0)
        }, 0)
        const notaFinal = leccionesUser.length ? Math.round((sum / leccionesUser.length) * 100) / 100 : 0
        user.notaFinal = notaFinal;
        user.save();

        // se crea el certificado
        ejs.renderFile(path.join(__dirname, '../views/', "certificado.ejs"), { user }, (err, data) => {
            if (err) {
                res.send(err);
            } else {
                // pdf.create(data, { format: 'Letter' }).toBuffer(function (err, buffer) {
                pdf.create(data, { format: 'Letter', orientation: 'landscape' }).toFile('uploads/certificados/' + user._id + '.pdf', function (err, buffer) {
                    if (err) {
                        res.send(err);
                    } else {
                        res.sendFile(path.resolve(buffer.filename));
                    }
                });
            }
        });
    } else {
        console.log('Error getCertificado leccionesUser ');
        res.send('Error');
    }
}

// obtener una leccionInstance por el id
export const getById = async (req, res) => {
    const leccion = await LeccionInstance.findById(req.params.leccionInstanceId);
    res.status(200).json(leccion)
}

// actualiza y asigna nueva leccionInstance si aprueba la leccion
export const updateById = async (req, res) => {

    const updateLeccion = await LeccionInstance.findByIdAndUpdate(req.params.leccionInstanceId, req.body, {
        new: true
    })

    const removeAccents = (str) => {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    }

    // se calcula la nota obtenida
    let nota = 0;
    updateLeccion.evaluacion.forEach(pregunta => {
        if (removeAccents(pregunta.respuesta.toLowerCase().trim()) === removeAccents(pregunta.respuestaCorrecta.toLowerCase().trim())) {
            nota += pregunta.nota;
        }
    });

    // si aprueba la leccion se le crea la siguiente leccion al usuario
    if (nota >= NOTA_MINIMA) {
        updateLeccion.nota = nota;
        updateLeccion.status = 'Completado';
        updateLeccion.save();

        const nextLeccion = await Leccion.findOne({ numero: updateLeccion.numero + 1 });

        if (nextLeccion) {
            const nextLeccionInstace = new LeccionInstance({
                nombre: nextLeccion.nombre,
                numero: nextLeccion.numero,
                recursos: nextLeccion.recursos,
                actividad: nextLeccion.actividad,
                niveles: nextLeccion.niveles,
                evaluacion: nextLeccion.evaluacion,
                user: updateLeccion.user
            });

            const user = await User.findById(updateLeccion.user);
            user.currentLeccion = nextLeccion.numero;
            await user.save();

            await nextLeccionInstace.save();

        } else {
            // termino el curso getCertificado(req.params.userId);
        }
    }

    res.status(200).json(updateLeccion)
}

// eliminar leccionInstance
export const deleteById = async (req, res) => {
    const { leccionInstanceId } = req.params
    await LeccionInstance.findByIdAndDelete(leccionInstanceId);
    res.status(204).json();
}