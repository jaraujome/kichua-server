import Leccion from '../models/Leccion';

// crear nueva leccion
export const create = async (req, res) => {
    const { numero, nombre, recursos, actividad, niveles, evaluacion } = req.body
    const newLeccion = new Leccion({ numero, nombre, recursos, actividad, niveles, evaluacion });

    const leccionSave = await newLeccion.save();

    res.status(201).json(leccionSave);
}

// obtener todas las lecciones
export const getLecciones = async (req, res) => {
    const lecciones = await Leccion.find().sort({ numero: 1 })
    res.status(200).json(lecciones)
}

// obtener leccion por id
export const getById = async (req, res) => {
    const leccion = await Leccion.findById(req.params.leccionId);
    res.status(200).json(leccion)
}

// modificar leccion
export const updateById = async (req, res) => {
    const updateLeccion = await Leccion.findByIdAndUpdate(req.params.leccionId, req.body, {
        new: true
    })
    res.status(200).json(updateLeccion)
}

// eliminar leccion
export const deleteById = async (req, res) => {
    const { leccionId } = req.params
    await Leccion.findByIdAndDelete(leccionId);

    // al eliminar se reordenan los numeros de las lecciones
    const lecciones = await Leccion.find().sort({ numero: 1 })
    lecciones.forEach(async (leccion, index) => {
        if (leccion.numero !== (index + 1)) {
            leccion.numero = (index + 1);
            await leccion.save();
        }
    });
    res.status(200).json(lecciones);
}
