import express from 'express';
import morgan from 'morgan';
import { createRoles, createSuperUser, createLeccions } from './libs/initialSetup';
import cors from 'cors';

import authRoute from './routes/auth.routes'
import leccionRoute from './routes/leccion.routes'
import leccionInstanceRoute from './routes/leccionInstance.routes'
import { extname } from 'path';

const app = express()

const fs = require('fs');
const path = require('path');
const multer = require('multer')
const upload = multer({ dest: 'uploads/' })

createRoles();
createSuperUser();
createLeccions();

app.use(cors())
app.use(morgan('dev'));
app.use(express.json());

app.use(express.static('uploads'));
app.use('/', authRoute);
app.use('/leccion', leccionRoute);
app.use('/leccionInstance', leccionInstanceRoute);
app.post('/upload/recurso', upload.single('recurso'), function (req, res, next) {
    console.log(req);
    if (req.file) {
        const ext = req.file.originalname.split('.').pop();
        const pathRename = path.resolve(req.file.destination, 'recursos/' + req.file.filename + '.' + ext);
        fs.rename(req.file.path, pathRename, function (err) {
            if (err) console.log(err);
        })
        res.json('recursos/' + req.file.filename + '.' + ext);
    } else {
        res.json('Error');
    }
})

app.get(/.*pdf$/, function (req, res) {
    console.log(path.resolve(__dirname, '../uploads/', req.originalUrl));
    if (fs.existsSync(path.resolve(__dirname, '../uploads/', req.originalUrl))) {
        res.sendFile(path.resolve(__dirname, '../uploads/', req.originalUrl));
    }
    res.json('Error');
});

app.get(/.*png$/, function (req, res) {
    if (fs.existsSync(path.resolve(__dirname, '../uploads/', req.originalUrl))) {
        res.sendFile(path.resolve(__dirname, '../uploads/', req.originalUrl));
    }
    res.sendFile(path.resolve(__dirname, '../uploads/', 'images/lecciones/leccion-1.png'));
});

export default app;