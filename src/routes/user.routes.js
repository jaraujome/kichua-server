import { Router } from 'express'
import * as authCtrl from '../controllers/auth.controller'
import { isAdmin } from '../middlewares/authJwt';

const router = Router()

router.use(isAdmin)

router.post('/users', authCtrl.getUsers)

export default router;