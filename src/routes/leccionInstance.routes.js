import { Router } from 'express'
import * as leccionInstanceCtrl from "../controllers/leccionInstance.controller";

const router = Router()

router.get('/', leccionInstanceCtrl.getLeccionInstances)

router.get('/user/:userId', leccionInstanceCtrl.getLeccionInstancesByUser)

router.get('/certificado/:userId', leccionInstanceCtrl.getCertificado)

router.get('/:leccionInstanceId', leccionInstanceCtrl.getById)

router.put('/:leccionInstanceId', leccionInstanceCtrl.updateById)

router.delete('/:leccionInstanceId', leccionInstanceCtrl.deleteById)

export default router;