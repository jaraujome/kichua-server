import { Router } from 'express'
const router = Router()

import * as leccionCtrl from "../controllers/leccion.controller";

router.post('/create', leccionCtrl.create)

router.get('/', leccionCtrl.getLecciones)

router.get('/:leccionId', leccionCtrl.getById)

router.put('/:leccionId', leccionCtrl.updateById)

router.delete('/:leccionId', leccionCtrl.deleteById)

export default router;